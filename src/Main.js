import React, { Component } from 'react';
import ReactNative from 'react-native';
import { Actions, Scene, Router, Drawer, ActionConst, Stack } from 'react-native-router-flux';
import Styles from './common/Styles';

// DRAWER
import SideMenu from './drawer/SideMenu';

import CheckUser from './scenes/auth/CheckUser'
import Menu from './scenes/Menu';
import AddRecipe from './scenes/AddRecipe';
import ChooseTags from './scenes/ChooseTags';

var ROUTES = {
  menu: Menu,
  addRecipe: AddRecipe,
  chooseTags: ChooseTags
};

class Main extends Component {

  render() {
    return (
      <Router>
        <Drawer key="drawer"
          contentComponent={SideMenu}
          drawerImage={require('../img/icon-hamburger.png')}
          leftButtonIconStyle={Styles.leftButtonIconStyle}
          drawerWidth={300}>
          <Scene key="main">
              <Scene key="checkuser"
                initial
                component={CheckUser}
                panHandlers={null}
                direction="vertical"
              />
              <Scene key="menu"
                component={Menu}
                navigationBarStyle={Styles.navBar}
                rightButtonImage={require('../img/icon-add.png')}
                rightButtonIconStyle={Styles.rightButtonIconStyle}
                //onRight={() => Actions.refresh({add:true})}
                onRight={() => Actions.addRecipe()}
                panHandlers={null}
                direction="vertical"
                />

              <Scene key="addRecipe"
                component={AddRecipe}
                panHandlers={null}
                hideNavBar
                />
              <Scene key="chooseTags"
                component={ChooseTags}
                panHandlers={null}
                navigationBarStyle={Styles.navBarLight}
                leftButtonIconStyle={Styles.leftButtonIconStyle}
                backButtonImage={require('../img/icon-back.png')}
                back
                title={'Choose tags'}
                titleStyle={Styles.navBarTitle}
                //onBack={() => Actions.refresh({popMe:true})}
                />

            </Scene>
        </Drawer>
      </Router>
    )
  }

}

export default Main;
