var AsyncStorage = require('./AsyncStorage');

const shoppingListKey = '@shoppingListData';
let shoppingListData;

module.exports = {

  async getShoppingList() {
    try {
      shoppingListData = await AsyncStorage.getObject(shoppingListKey);
      if (shoppingListData !== null) {
        return shoppingListData;
      } else {
        return [];
      }
    } catch(e) {
      console.log(e);
    }
  },

  search(nameKey, myArray) {
    for (var i=0; i < myArray.length; i++) {
      if (myArray[i].ingredient === nameKey) {
        //return myArray[i];
        return i
      }
    }
  },

  async addToShoppingList(ingredients) {

    //console.log(ingredients)
    let shoppingListData;
    try {
      shoppingListData = await this.getShoppingList();
    } catch(e) {
      console.log(e);
    }
    
    if (shoppingListData.length > 0) {
      ingredients.forEach(ingredient => {
        //console.log(this.search(ingredient.ingredient, shoppingListData));
        let searchResult = this.search(ingredient.ingredient, shoppingListData);
        //console.log(searchResult, 'searchResult');
        if (searchResult > -1) {
          const existingQuantityString = shoppingListData[searchResult].quantity;
          const newQuantityString = ingredient.quantity;
          //console.log('does it have a letter in it?', quantityString, quantityString.match(/[a-z]/i))
          var existingStringResultArray = existingQuantityString.match(/[a-z]/i);
          var newStringResultArray = newQuantityString.match(/[a-z]/i);
          //console.log(existingStringResultArray, newStringResultArray)
          if (existingStringResultArray && newStringResultArray) {
            existingQuantityNum = parseInt(existingQuantityString.substring(0, existingStringResultArray.index));
            newQuantityNum = parseInt(newQuantityString.substring(0, newStringResultArray.index));
            measurement = existingQuantityString.substring(existingStringResultArray.index);
            shoppingListData[searchResult].quantity = (existingQuantityNum + newQuantityNum) + ' ' + measurement;
          } else {
            var newQuantity = parseInt(existingQuantityString) + parseInt(newQuantityString);
            shoppingListData[searchResult].quantity = newQuantity.toString();
          }          
        } else {
          //console.log('pushing', ingredient)
          shoppingListData.push(ingredient);
        }
      });

      //console.log('data to store, data already');
      try {
        await AsyncStorage.storeObject(shoppingListKey, shoppingListData); 
      } catch(e) {
        console.log('error saving data', e);
      }
    } else {
      //console.log('new data');
      shoppingListData = ingredients;
      try {
        await AsyncStorage.storeObject(shoppingListKey, shoppingListData); 
      } catch(e) {
        console.log('error saving data', e);
      }
    }
  },

  async clearShoppingList() {
    await AsyncStorage.storeObject(shoppingListKey, null);
  },

  async removefromShoppingList(index) {
    let shoppingListData;
    try {
      shoppingListData = await this.getShoppingList();
    } catch(e) {
      console.log(e);
    }
    shoppingListData.splice(index, 1);
    try {
      await AsyncStorage.storeObject(shoppingListKey, shoppingListData); 
    } catch(e) {
      console.log('error saving data', e);
      return false;
    }    
    return true;
  }

};