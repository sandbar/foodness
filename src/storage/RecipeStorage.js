var AsyncStorage = require('./AsyncStorage');

const RECIPES_KEY = '@RecipeData';
var RECIPES_DATA;

module.exports = {

  async getRecipes() {
    if(RECIPES_DATA == null) {
      RECIPES_DATA = await this.loadRecipes();
    }
    return RECIPES_DATA;
  },

  async loadRecipes() {
    var recipes = await AsyncStorage.getObject(RECIPES_KEY);
    if (recipes != null) {
      return recipes;
    } else {
      return [];
    }
  },

  async storeRecipe(recipe) {

    if (RECIPES_DATA == null) {
      RECIPES_DATA = await this.loadRecipes();
    }

    // console.log('RECIPES_DATA', RECIPES_DATA);

    // create new ID
    if (RECIPES_DATA.length > 0) {
      var lastID = RECIPES_DATA[parseInt(RECIPES_DATA.length - 1)].id;
      newID = parseInt(lastID + 1);
    } else {
      newID = 1;
    }
    recipe.id = newID;

    RECIPES_DATA.push(recipe);

    return await AsyncStorage.storeObject(RECIPES_KEY, RECIPES_DATA);

  }

}
