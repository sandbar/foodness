import firestore from '@react-native-firebase/firestore';
import auth from '@react-native-firebase/auth';
import storage from '@react-native-firebase/storage';
import UserProxy from '../data/UserProxy';

module.exports = {

  /** AUTH **/

  async linkCredentialsToAnon(email, password) {
    const credential = auth.EmailAuthProvider.credential(email, password);
    //console.log(credential);
    return auth.currentUser.linkWithCredential(credential)
    .then((usercred) => {
      console.log(usercred)
      return usercred;
    })
    .catch((e) => {
      return e;
    })
  },

  async sendSignInLinkToEmail(email, actionCodeSettings) {
    try {
      await auth().sendSignInLinkToEmail(email, actionCodeSettings)
    } catch (e) {
      console.log(e);
    }
   
    // .then(function() {
    //   console.log('The link was successfully sent')
    //   // The link was successfully sent. Inform the user.
    //   // Save the email locally so you don't need to ask the user for it again
    //   // if they open the link on the same device.
    //   //window.localStorage.setItem('emailForSignIn', email);
    //   //UserProxy.setEm
    // })
    // .catch(function(error) {
    //   console.log(error)
    //   // Some error occurred, you can inspect the code: error.code
    // }); 
  },
 

  /** SUBSCRIPTIONS **/

  async updateSubscription(purchaseRecipt) {
    const userRef = firestore().collection('users').doc(UserProxy.getUID());
    return userRef.update({
      subscriptionReceipt: purchaseRecipt
    })
    .then (() => {
      console.log('receipt added')
      return true;
    })
    .catch((error) => {
      console.log('error adding receipt', error)
      return false;
    })     
  },

  /** RECIPES **/

  async addToMyRecipes(recipe, folderChosen, isShared) {
    const recipeCollectionRef = firestore().collection('users').doc(UserProxy.getUID()).collection('myRecipes'); 
    return recipeCollectionRef.add({
      recipe,
      likes: 0,
      folder: folderChosen,
      isPremium: false,
      isShared: isShared
    })
    .then((docRef) => docRef.id)
    .catch((e) => console.log(e)) 
  },

  async editMyRecipes(recipeID, recipe, folderChosen, isShared) {
    const recipeDocRef = firestore().collection('users').doc(UserProxy.getUID()).collection('myRecipes').doc(recipeID); 
    return recipeDocRef.update({
      recipe,
      folder: folderChosen,
      isShared: isShared
    })
    .then(() => { return true; })
    .catch((e) => { console.log(e); return false; })
  },

  async moveToMyRecipes(recipe, recipeID) {
    console.log(UserProxy.getUID())
    const recipeCollectionRef = firestore().collection('users').doc(UserProxy.getUID()).collection('myRecipes'); 
    return recipeCollectionRef.doc(recipeID).set({
      recipe,
      folder: 0,
      likes: 0,
      isShared: false,
      isPremium: true
    })
    .then(() => recipeID)
    .catch((e) => console.log(e))
  },

  async shareRecipe(recipeID, recipe) {
    recipe.screenName = UserProxy.getScreenName();
    recipe.userImage = UserProxy.getUID();
    const sharedRecipeCollectionRef = firestore().collection('sharedRecipes');
    return sharedRecipeCollectionRef.doc(recipeID).set({
      recipe,
      likes: 0
    })
    .then(function() {
      console.log('recipe shared!');
      return true;
    })
    .catch(function(error) {
      console.error('Error sharing recipe:', error);
      return false;
    }); 
  },

  async unShareRecipe(recipeID) {
    const sharedRecipeCollectionRef = firestore().collection('sharedRecipes');
    return sharedRecipeCollectionRef.doc(recipeID).get()
    .then(function(doc) {
      if (doc.exists) {
        return sharedRecipeCollectionRef.doc(recipeID).delete()
        .then(function() {
          return true;
        })
        .catch(function(error) {
          console.error('Error deleting sharing recipe:', error);
          return false;
        })            
      } else {
        return true;
      }
    }); 
  },

  async deleteRecipe(recipeID) {
    const docRef = firestore().collection('users').doc(UserProxy.getUID()).collection('myRecipes').doc(recipeID);   
    return docRef.delete()
    .then(() => { console.log('recipe deleted!'); return true })
    .catch(e => {console.log(e); return false}); 
  },

  async deleteImage(recipeID) {
    const imageRef = storage().ref(`recipe-images/${recipeID}.jpg`)
    return imageRef.delete()
    .then(() => { console.log('image deleted!'); return true; })
    .catch((e) => { console.log(e); return false; }); 
  },


  /** IMAGES **/

  async uploadImage(recipeID, imageData) {
    const ext = imageData.uri.split('.').pop(); // Extract image extension
    const filename = `${recipeID}.${ext}`;
    return storage()
      .ref(`recipe-images/${filename}`)
      .putFile(imageData.uri)    
      .on(
        storage.TaskEvent.STATE_CHANGED,
        snapshot => {
          let state = {};
          state = {
            ...state,
            progress: (snapshot.bytesTransferred / snapshot.totalBytes) * 100 // Calculate progress percentage
          };
          if (snapshot.state === storage.TaskState.SUCCESS) {
            console.log('image upload success!');
            return true;
          }
        },
        error => {
          alert('Sorry, Try again.');
        }
      );  
  },

  async uploadUserImage(userID, imageData) {
    const ext = imageData.uri.split('.').pop(); // Extract image extension
    const filename = `${userID}.${ext}`;
    return storage()
      .ref(`user-images/${filename}`)
      .putFile(imageData.uri)    
      .on(
        storage.TaskEvent.STATE_CHANGED,
        snapshot => {
          let state = {};
          state = {
            ...state,
            progress: (snapshot.bytesTransferred / snapshot.totalBytes) * 100 // Calculate progress percentage
          };
          if (snapshot.state === storage.TaskState.SUCCESS) {
            console.log('image upload success!');
            return true;
          }
        },
        error => {
          alert('Sorry, Try again.');
        }
      );  
  },  

  /** FOLDERS **/

  async updateFolders(folderChosen, originalFolder) {
    let userFolders = UserProxy.getFolders();
    if (originalFolder) 
      userFolders[originalFolder].total -= 1;
    userFolders[folderChosen].total += 1;
    const userRef = firestore().collection('users').doc(UserProxy.getUID());
    return userRef.update({
      myFolders: userFolders
    })
    .then (() => {
      console.log('user folders updated successfully')
      return true;
    })
    .catch((error) => {
      console.log('error updating user folders', error)
      return false;
    })     
  },

  async updateFoldersDeleteRecipe(folderChosen) {
    let userFolders = UserProxy.getFolders();
    userFolders[folderChosen].total -= 1;
    const userRef = firestore().collection('users').doc(UserProxy.getUID());
    return userRef.update({
      myFolders: userFolders
    })
    .then (() => {
      console.log('user folders updated successfully')
      return true;
    })
    .catch((error) => {
      console.log('error updating user folders', error)
      return false;
    })     
  },

  /** PROFILE **/

  async addProfileImageStatus() {
    const userRef = firestore().collection('users').doc(UserProxy.getUID());
    const userJson = UserProxy.getUserData();
    console.log('1', userJson)
    userJson.photoURL = UserProxy.getUID();
    console.log('2', userJson)
    UserProxy.storeUserData(userJson);
    return userRef.update({userJson})
    .then(() => {
      console.log('image status updated')
      return true;
    })
    .catch((e) => {
      console.log(e);
      return false;
    })
  },

  async addUsername(screenName, edited) {
    return firestore().collection('usernames')
    .doc(screenName)
    .set({username:screenName, uid:UserProxy.getUID()})
    .then(() => {
      if (edited) {
        return firestore().collection('usernames').doc(UserProxy.getScreenName())
        .delete()
        .then(() => {
          return true;
        })
        .catch((e) => {
          console.log(e);
          return false;
        })
      } else {
        return true;
      }
    })
    .catch((e) => {
      console.log(e);
      return false;
    });
  },  

  async addScreenName(screenName) {
    const userRef = firestore().collection('users').doc(UserProxy.getUID());
    const userJson = UserProxy.getUserData();
    userJson.displayName = screenName;
    UserProxy.storeUserData(userJson);
    //console.log({userJson})
    return userRef.update({userJson})
    .then(() => {
      return true;
    })
    .catch((e) => {
      console.log(e);
      return false;
    })    
  },

};