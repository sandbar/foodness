import { Platform } from 'react-native';
import * as RNIap from 'react-native-iap';

const itemSubs = Platform.select({
  ios: [
    'subscription.monthly'
  ],
  android: [
    'subscription.monthly'
  ]
});

module.exports = {

  async getSubscriptions() {
    try {
      const products = await RNIap.getSubscriptions(itemSubs);
      //console.log('Products', products);
      return products;
    } catch (err) {
      console.warn(err.code, err.message);
      return [];
    }
  },

  async validateReceipt(receiptBody) {
    try {
      const result = await RNIap.validateReceiptIos(receiptBody, true);
      return result;
    } catch (err) {
      console.warn(err.code, err.message);
      return [];
    }
  }

}