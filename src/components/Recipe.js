import React, { Component } from 'react';
import PropTypes from 'prop-types';
import {
  View,
  Text,
  StyleSheet,
  Image,
  TouchableWithoutFeedback,
  LayoutAnimation,
  ImageBackground,
  TouchableOpacity,
  ScrollView,
  Platform
} from 'react-native';

import Styles from '../common/Styles';
import Colors from '../common/Colors';
import AsyncImage from './AsyncImage';

const {width, height} = require('Dimensions').get('window');
const iPhoneXMargin = (Platform.OS === 'ios') && (height === 812 || height === 896) ? 32 : 0;

// const selectOpened = (opened, style) => opened ? style.opened : style.closed;

// const getStyles = (opened) => StyleSheet.create({
//   recipeCard: selectOpened(opened, {
//     opened: {
//       height: height,
//       width: width,
//       position: 'relative',
//       zIndex: 1,
//       margin: 0,
//       backgroundColor: Colors.brightGreen
//     },
//     closed: {
//       width: width - 20,
//       backgroundColor: '#fff',
//       padding: 10,
//       borderRadius: 4,
//       marginBottom: 10,
//       shadowColor: "#000",
//       shadowOpacity: 0.2,
//       shadowRadius: 3,
//       shadowOffset: {
//         height: 3,
//         width: 0
//       },
//       elevation: 1,
//       marginHorizontal: 10,
//       marginBottom: 10,
//     },
//   }),
//   title: selectOpened(opened, {
//     opened: {
//       height: 0,
//       overflow: 'hidden'
//     },
//     closed: {

//     }
//   }),
//   recipeCardTitle: selectOpened(opened, {
//     opened: {
//       fontSize: 22,
//       color: '#fff'
//     },
//     closed: {
//       fontSize: 16,
//     }
//   }),
//   recipeCardImage: selectOpened(opened, {
//     opened: {
//       width: width,
//       height: width * 0.6,
//     },
//     closed: {
//       width: width - 40,
//       height: width * 0.4,
//       marginTop: 5,
//     }
//   }),
//   favourite: selectOpened(opened, {
//     opened: { opacity:1 },
//     closed: { opacity:1 }
//   }),
//   close: selectOpened(opened, {
//     opened: {opacity:1, width:16, height:16},
//     closed: {position: 'absolute', opacity:0, right:0, width:16, height:16}
//   }),

// });

class Recipe extends Component {

  // static propTypes = {
  //   recipe: PropTypes.object.isRequired,
  //   index: PropTypes.oneOfType([
  //     PropTypes.number,
  //     PropTypes.string,
  //   ]),
  //   onPressOpen: PropTypes.func,
  //   onPressClose: PropTypes.func,
  //   opened: PropTypes.bool,
  //   noAnimation: PropTypes.bool,
  // }

  // static defaultProps = {
  //   index: 0,
  //   onPress: () => {}
  // }

  constructor(props) {
    super(props);
    this.state = { opened: false };

    //this.onPressOpen = this.onPressOpen.bind(this);
    //this.onPressClose = this.onPressClose.bind(this);
  }

  // componentWillUpdate() {
  //   const animation = LayoutAnimation.create(500, 'easeInEaseOut', 'opacity');
  //   LayoutAnimation.configureNext(animation);
  // }

  // onPressOpen() {
  //   if (!this.state.opened) {
  //     this.props.onPressOpen();
  //     //Actions.refresh({hideNavBar:true});
  //     this.props.onPress();
  //     if (this.props.noAnimation) {
  //       return;
  //     }
  //     this.refs.recipeCard.measureInWindow((x, y) => {
  //       //this.refs.close.fadeInRight();
  //       //this.refs.recipeTitle.fadeIn();
  //       //setTimeout(() => this.refs.description.fadeInUpBig(), 400);
  //       this.setState({
  //         opened: true,
  //         windowPos: { x, y },
  //       });
  //     });

  //   }
  // }

  // onPressClose() {
  //   if (this.state.opened) {
  //     this.props.onPressClose();
  //     //this.refs.nextEpisode.fadeOutLeft();
  //     //this.refs.description.fadeOutDownBig();
  //     //this.refs.close.fadeOutRight();
  //     //Actions.refresh({hideNavBar:false});
  //     return  this.setState({opened: false, windowPos: null});
  //   }
  // }

  render() {
    const props = this.props;
    const { opened } = this.state;
    const styles = getStyles(opened)
    // console.log(this.state.windowPos ? - this.state.windowPos.y : '')
    const containerPos = this.state.windowPos ? {
      top: - this.state.windowPos.y + (width * 0.4) - (iPhoneXMargin)
    } : {};

    return (
      <View style={[Styles.recipeCard, styles.recipeCard, containerPos]} ref="recipeCard">
        <TouchableWithoutFeedback onPress={this.onPressOpen}>
        <View>
            <Animatable.View
              style={[styles.title, {flexDirection:'row', alignItems:'center'}]}
              duration={800}
              ref="recipeTitle"
              delay={400}
            >
              <Text style={[Styles.recipeCardTitle, styles.recipeCardTitle]} numberOfLines={1}>{props.recipe.title}</Text>
              <TouchableOpacity 
                //onPress={() => Actions.addRecipe({recipeID: props.index, edit:true})}
                onPress={() => this.props.navigation.navigate('AddRecipe', {recipeID: props.index, edit:true})}
              >
                <Image 
                  style={Styles.recipeEdit}
                  source={require('../../img/icon-edit.png')} />
              </TouchableOpacity>
              <Image
                style={[Styles.recipeCardFavourite, styles.favourite]}
                source={props.recipe.isFavourite ? require('../../img/icon-favourite-on.png') : require('../../img/icon-favourite-off.png')} />
            </Animatable.View>
          <ScrollView>
              {opened ? this.renderOpened(props, styles) : this.renderClosed(props, styles)}
          </ScrollView>
        </View>
        </TouchableWithoutFeedback>
      </View>
    )
  }

  renderClosed(props, styles) {
    return (
      <AsyncImage 
        index={props.index} 
        styles={styles}
        opened={this.state.opened} />
    )
  }

  renderOpened(props, styles) {
    return (
      <View onStartShouldSetResponder={() => true}>
        <AsyncImage
          index={props.index} 
          styles={styles}
          opened={this.state.opened}
          onPressClose={this.onPressClose} />
        <View style={[Styles.paddedContainer, {backgroundColor: 'white'}]}>
          <Text style={Styles.defaultMainTitle}>{props.recipe.title}</Text>
        </View>
        <View style={[Styles.paddedContainer, {flexDirection:'row', backgroundColor: 'white'}]}>
          {this.renderTags(props.recipe.tags)}
        </View>
        <View style={[Styles.paddedContainer, {paddingTop:0, backgroundColor: 'white'}]}>
          <View style={{flexDirection: 'row', height: 40, alignItems: 'center'}}>
            <Text style={[Styles.defaultTitle, {color:'#000', flex:1}]}>Ingredients</Text>
            <TouchableOpacity>
              <Image source={require('../../img/icon-shoppinglist.png')} />
            </TouchableOpacity>
          </View>
        </View>
        {this.renderIngredients(props.recipe.ingredients)}
        <View style={[Styles.paddedContainer, {flex:1, backgroundColor: 'white'}]}>
          <View style={{flexDirection: 'row', height: 40, alignItems: 'center'}}>
            <Text style={[Styles.defaultTitle, {color:'#000', flex:1}]}>Method</Text>
          </View>
          <Text style={[Styles.defaultText, {color:'#000', lineHeight: 30, flex:1}]}>{props.recipe.method}</Text>
        </View>
        <View style={[Styles.paddedContainer, {backgroundColor:Colors.brightGreen, alignItems: 'center'}]}>
          <TouchableOpacity style={[Styles.button, {paddingLeft:0, marginBottom: 20, marginTop: 5}]}>
            <Text style={[Styles.buttonText, {flex:1, textAlign:'center'}]}>Start cooking</Text>
          </TouchableOpacity>
          <Image source={require('../../img/leaves.png')} style={{marginBottom: 20}} />
        </View>
      </View>
    )
  }

  renderTags(tags) {
    if (tags) {
      if (tags.length > 0) {
        var that = this;
        return tags.map(function(tag, i){
          if (tag.selected) {
            return (
              <View key={i} style={{overflow:'visible'}}>
                <TouchableOpacity style={Styles.tag}
                  onPressIn={(evt) => that.handleTagPress(true, tag.tag, evt, i)}
                  onPressOut={(evt) => that.handleTagPress(false, tag.tag, evt, i)}>
                  <Text style={[Styles.defaultText, {fontSize:12}]}>{tag.displayText}</Text>
                </TouchableOpacity>
              </View>
            )
          }
        })
      }
    }
  }

  renderIngredients(ingredients) {
    if (ingredients) {
      return ingredients.map( function(ingredient, i) {
        return (
          <View style={[Styles.ingredientRow, {backgroundColor: i % 2 == 0 ? Colors.lightestGrey : Colors.altLightestGrey}]} key={i}>
            <Text style={[Styles.defaultText, {color:'#000', width:80, paddingLeft:10}]}>{ingredient.quantity}</Text>
            <Text style={[Styles.defaultText, {color:'#000'}]}>{ingredient.ingredient}</Text>
          </View>
        )
      })
    }
  }

}

export default Recipe;
