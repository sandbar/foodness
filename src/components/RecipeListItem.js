import React, { useState } from 'react';
import {
  View,
  Text,
  Image,
  TouchableOpacity,
} from 'react-native';

import firestore from '@react-native-firebase/firestore';

import UserProxy from '../data/UserProxy';
import Styles from '../common/Styles';
import AsyncImage from './AsyncImage';
import AsyncUserImage from './AsyncUserImage';
import DataConstants from '../data/DataConstants';

const RecipeListItem = (props) => {

  const {item, myRecipe, collectionRecipe, collectionID} = props;
  const navigation = props.navigation;
  const recipe =  item.recipe;
  const recipeID = item.key;
  const folder = item.folder;
  const isShared = item.isShared;
  const isPremium = item.isPremium;
  const tagsAll = DataConstants.getTags();
  const userID = UserProxy.getUID();
  const subscriber = UserProxy.getIsSubscribed();
  //const likes = item.likes;

  const [isLiked, setIsLiked] = useState(item.isLiked);
  const [likes, setLikes] = useState(item.likes);

  const likeIt = () => {
    setIsLiked(true);
    setLikes(likes + 1);
    sendToServer();
  }

  const unLikeIt = () => {
    setIsLiked(false);
    setLikes(likes - 1);
    sendToServer();
  }

  const sendToServer = () => {

    let collectionRef;
    if (myRecipe) {
      collectionRef = firestore().collection('users').doc(userID).collection('myRecipes');
    } else if (collectionRecipe) {
      collectionRef = firestore().collection('recipeCollections').doc(collectionID).collection('recipes');
    } else {
      collectionRef = firestore().collection('sharedRecipes');
    }

    collectionRef.doc(recipeID)
    .update({
      likes: isLiked ? firestore.FieldValue.increment(-1) : firestore.FieldValue.increment(1)
    })
    .then(() => { 
      const userRef = firestore().collection('users').doc(userID);
      userRef.update({
        myLikedRecipes: isLiked ?  firestore.FieldValue.arrayRemove(recipeID) : firestore.FieldValue.arrayUnion(recipeID)
      })
      .then(function() {
      })
      .catch(function(error) {
        console.error('Error updating user document:', error);
      });   
     })
    .catch((e) => {console.log('error updating recipe', e)});

  }

  const navigateToScreen = () => {
    // if (isPremium && !UserProxy.getIsSubscribed() && !myRecipe) {
    //   navigation.navigate('Subscribe', {view:'ViewRecipe', viewParams:{recipe: recipe, index: recipeID, collectionRecipe: collectionRecipe,myRecipe: myRecipe}});
    // } else {
      navigation.navigate('ViewRecipe', { 
        recipe: recipe, 
        index: recipeID, 
        collectionRecipe: collectionRecipe,
        myRecipe: myRecipe });       
    //}
  }; 

  return (
    <View style={Styles.recipeCard}>
      <TouchableOpacity onPress={() => { navigateToScreen() }}>
        {recipe.isShared &&
        <View style={{flexDirection:'row', alignItems: 'center', padding: 10}}>
          <AsyncUserImage imageData={recipe.userImage} />
          <Text>{recipe.screenName}</Text>
        </View>
        }
        <AsyncImage index={recipeID} title={recipe.title} hasImage={recipe.hasImage}  />
      </TouchableOpacity>
      <View style={{flexDirection:'row', alignItems:'center', flex:1, margin: 10}}>
        <View style={{flexDirection: 'row', alignItems: 'center', flex:1}}>
          {recipe.tags && recipe.tags.length > 0 &&
            recipe.tags.map(function(tag, i) {
              return (
                <Image source={tagsAll[tag].icon} style={{marginRight: 10}} key={i} />
              )}) 
          }
        </View>          

        {myRecipe &&
          <TouchableOpacity 
            onPress={() => navigation.navigate('AddRecipe', {
              recipeData: recipe, 
              recipeID: recipeID, 
              edit: true, 
              folderEdit: folder, 
              isPremium: isPremium,
              isSharedEdit: isShared})}>
            <Image 
              style={Styles.recipeEdit}
              source={require('../../img/icon-edit.png')} />
          </TouchableOpacity>           
        }
        {!myRecipe &&
          <TouchableOpacity 
            style={{flexDirection: 'row'}}
            onPress={() => { isLiked ? unLikeIt() : likeIt() }}>
            <Text style={Styles.smallText}>{likes !== 1 ? `${likes} Likes` : `${likes} Like`}</Text>
            <Image
              style={Styles.recipeCardLiked}
              source={isLiked ? require('../../img/icon-favourite-on.png') : require('../../img/icon-favourite-off.png')} />
          </TouchableOpacity>          
        }
        
      </View>        
    </View>      
  )


}

export default RecipeListItem;