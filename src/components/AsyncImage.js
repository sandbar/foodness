import React from 'react';
import {  View, ActivityIndicator, ImageBackground, Text  } from 'react-native';
import storage from '@react-native-firebase/storage';
import Styles from '../common/Styles';

export default class AsyncImage extends React.Component {

  constructor(props, context) {
    super(props, context);
    this.state = {
      loading: true,
      mounted: true,
      isSplash: this.props.isSplash,
      imageIndex: this.props.index,
      title: this.props.title,
      hasImage: this.props.hasImage
    };
  }

  componentDidMount() {
    this.setState({ isMounted: true });
    this.state.hasImage ? this.getAndLoadHttpUrl() : this.setState({ loading: false });
  }

  componentWillUnmount() {
    //this.setState({ isMounted: false })
  }  

  async getAndLoadHttpUrl() {
    if (this.state.mounted == true) {
      var ref = storage().ref(`recipe-images/${this.state.imageIndex}.jpg`);
      ref.getDownloadURL().then(data => {
        //console.log(data);
          this.setState({ url: data });
          this.setState({ loading: false });
      }).catch(error => {
          this.setState({ loading: false });
      })
    }
  }  

  render() {
    if (this.state.mounted == true) {
      if (this.state.loading == true) {
        return (
          <View key={this.props.image} style={[this.state.isSplash ? Styles.recipeImageSplash : Styles.recipeCardImage, { alignItems: 'center', justifyContent: 'center' }]} >
            <ActivityIndicator />
          </View>
        )
      }
      else {
        const imgSource = this.state.hasImage ? {uri:this.state.url} : require('../../img/recipe-image-default.png')
        return (
          <ImageBackground source={imgSource} style={this.state.isSplash ? Styles.recipeImageSplash : Styles.recipeCardImage}>
            {this.state.title &&
              <View style={Styles.recipeCardTitle}>
                <Text style={Styles.recipeCardTitleText}>{this.state.title}</Text>
              </View>
            }
          </ImageBackground>        
        )
      }
    }
    else {
      return null
    }
  }  

}

module.exports = AsyncImage;