import React from 'react';
import {  View, ActivityIndicator, Image } from 'react-native';
import storage from '@react-native-firebase/storage';
import Styles from '../common/Styles';

export default class AsyncUserImage extends React.Component {

  constructor(props, context) {
    super(props, context);
    this.state = {
      loading: true,
      mounted: true,
      hasImage: false,
      imageData: props.imageData
    };
  }

  componentDidMount() {
    this.setState({ isMounted: true });
    this.getAndLoadHttpUrl();
  }

  componentWillUnmount() {
    //this.setState({ isMounted: false })
  }  

  async getAndLoadHttpUrl() {
    if (this.state.mounted == true) {
      var ref = storage().ref(`user-images/${this.state.imageData}.jpg`);
      ref.getDownloadURL().then(data => {
        //console.log(data);
          this.setState({ 
            url: data,
            loading: false,
            hasImage: true
          });
      }).catch(error => {
          this.setState({ loading: false });
      })
    }
  }  

  render() {
    if (this.state.mounted == true) {
      if (this.state.loading == true) {
        return (
          <View key={this.props.image} style={Styles.collectionCardImage} >
            <ActivityIndicator />
          </View>
        )
      }
      else {
        const imgSource = this.state.hasImage ? {uri:this.state.url} : require('../../img/leaves.png')
        return (
          <View style={[Styles.sharedUserPhoto, {marginRight: 6}]}>
            <Image source={imgSource} style={this.state.hasImage ? Styles.sharedUserPhoto : Styles.defaultSharedUserPhoto} />
          </View>               
        )
      }
    }
    else {
      return null
    }
  }  

}

module.exports = AsyncUserImage;