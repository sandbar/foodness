import React, {useState, useEffect} from 'react';
import {
  Alert, View
} from 'react-native';

import PushNotificationIOS from "@react-native-community/push-notification-ios";

export const Notifications = () => {

  const [permissions, setPermissions] = useState({});

  useEffect(() => {
    PushNotificationIOS.requestPermissions();
    PushNotificationIOS.addEventListener('register', onRegistered);
    PushNotificationIOS.addEventListener('registrationError', onRegistrationError);
    PushNotificationIOS.addEventListener('localNotification', onLocalNotification);
    return () => {
      PushNotificationIOS.removeEventListener('register', onRegistered);
      PushNotificationIOS.removeEventListener('registrationError', onRegistrationError);
      PushNotificationIOS.removeEventListener('localNotification', onLocalNotification);
    };
  }, []);

  const onRegistered = deviceToken => {
    Alert.alert('Registered For Remote Push', `Device Token: ${deviceToken}`, [
      {
        text: 'Dismiss',
        onPress: null,
      },
    ]);
  };

  const onRegistrationError = error => {
    Alert.alert(
      'Failed To Register For Remote Push',
      `Error (${error.code}): ${error.message}`,
      [
        {
          text: 'Dismiss',
          onPress: null,
        },
      ],
    );
  };

  const onLocalNotification = notification => {
    Alert.alert(
      'Local Notification Received',
      'Alert message: ' + notification.getMessage(),
      [
        {
          text: 'Dismiss',
          onPress: null,
        },
      ],
    );
  };

  return (
    <View />
  )

}




