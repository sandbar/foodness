import React from 'react';
import { View, TouchableOpacity, Image } from 'react-native';

import AsyncCollectionImage from './AsyncCollectionImage';
import DataConstants from '../data/DataConstants';
import Styles from '../common/Styles';

const CollectionListItem = (props) => {

  const {item, navigation, index} = props;
  const collectionID = item.key;
  const collectionHasImage = item.hasImage;
  const collectionTitle = item.title;
  const collectionTags = item.tags;
  const collectionIndex = index;
  const tagsAll = DataConstants.getTags();

  return (
    <View style={Styles.collectionCard}>
      <TouchableOpacity 
        onPress={() => { navigation.navigate('CollectionRecipes', { 
          index: collectionID, 
          title: collectionTitle,
          collectionIndex: collectionIndex }); }}>
        <AsyncCollectionImage index={collectionID} title={collectionTitle} hasImage={collectionHasImage} />
      </TouchableOpacity>
      <View style={{flexDirection:'row', alignItems:'center', flex:1, margin: 10}}>
        <View style={{flexDirection: 'row', alignItems: 'center', flex:1}}>
          {collectionTags && collectionTags.length > 0 &&
            collectionTags.map(function(tag, i) {
              return (
                <Image source={tagsAll[tag].icon} style={{marginRight: 10}} key={i} />
              )}) 
          }
        </View>            
      </View>
    </View>
  )

};

export default CollectionListItem;