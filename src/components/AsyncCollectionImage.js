import React from 'react';
import {  View, ActivityIndicator, ImageBackground, Text  } from 'react-native';
import storage from '@react-native-firebase/storage';
import Styles from '../common/Styles';

export default class AsyncCollectionImage extends React.Component {

  constructor(props, context) {
    super(props, context);
    this.state = {
      loading: true,
      mounted: true,
      isSplash: this.props.isSplash,
      imageIndex: this.props.index,
      title: this.props.title,
      hasImage: this.props.hasImage
    };
  }

  componentDidMount() {
    this.setState({ isMounted: true });
    this.state.hasImage ? this.getAndLoadHttpUrl() : this.setState({ loading: false });
  }

  componentWillUnmount() {
    //this.setState({ isMounted: false })
  }  

  async getAndLoadHttpUrl() {
    if (this.state.mounted == true) {
      var ref = storage().ref(`collection-images/${this.state.imageIndex}.jpg`);
      ref.getDownloadURL().then(data => {
        //console.log(data);
          this.setState({ url: data });
          this.setState({ loading: false });
      }).catch(error => {
          this.setState({ loading: false });
      })
    }
  }  

  render() {
    if (this.state.mounted == true) {
      if (this.state.loading == true) {
        return (
          <View key={this.props.image} style={Styles.collectionCardImage} >
            <ActivityIndicator />
          </View>
        )
      }
      else {
        const imgSource = this.state.hasImage ? {uri:this.state.url} : require('../../img/recipe-image-default.png')
        return (
          <ImageBackground source={imgSource} style={Styles.collectionCardImage} imageStyle={{ borderRadius: 8 }}>
            {this.state.title &&
              <View style={Styles.collectionCardTitle}>
                <Text style={Styles.collectionCardTitleText}>{this.state.title}</Text>
              </View>
            }
          </ImageBackground>        
        )
      }
    }
    else {
      return null
    }
  }  

}

module.exports = AsyncCollectionImage;