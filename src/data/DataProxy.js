let subscriptionData = [];

module.exports = {

  setSubscriptions(subData) {
    subscriptionData = subData;
  },

  getSubscriptions() {
    return subscriptionData;
  }

};