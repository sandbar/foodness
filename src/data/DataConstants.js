var tags = [
  {id: 0, tag:'Vegetarian', displayText:'vegetarian', icon:require('../../img/icon-vg.png')},
  {id: 1, tag:'Vegan', displayText:'vegan', icon:require('../../img/icon-ve.png')},
  {id: 2, tag:'Gluten Free', displayText:'gluten free', icon:require('../../img/icon-gf.png')},
  {id: 3, tag:'Paleo', displayText:'paleo', icon:require('../../img/icon-pl.png')},
  {id: 4, tag:'Ketogenic', displayText:'ketogenic', icon:require('../../img/icon-kt.png')},
  {id: 5, tag:'Pescetarian', displayText:'pescetarian', icon:require('../../img/icon-ps.png')},
];
//Low Histermine, high iron, elminiation

var folderColors = [
  {id: 0, color: '#ED7341'},
  {id: 1, color: '#8CCB44'},
  {id: 2, color: '#DBB500'},
  {id: 3, color: '#AC5875'},
  {id: 4, color: '#E163F1'},
  {id: 5, color: '#22AADC'}
];

module.exports = {
  getTags() {
    return tags
  },

  getFolderColors() {
    return folderColors;
  }
}
