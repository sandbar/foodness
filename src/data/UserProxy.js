var userData = {};
var likedData = [];
var folderData = [];
var isSubscribed = false;

module.exports = {

  storeUserData: function(data) {
    userData = data;
  },

  getUserData() {
    return userData;
  },

  getUID() {
    return userData.uid;
  },

  getScreenName() {
    return userData.displayName;
  },

  getIsAnonymous() {
    return userData.isAnonymous;
  },

  getPhotoURL() {
    return userData.photoURL;
  },

  storeLiked: function(data) {
    likedData = data;
  },

  getLiked() {
    return likedData;
  },

  storeFolders: function(data) {
    folderData = data;
  },

  getFolders() {
    return folderData;
  },

  getFolderTotal(folderIndex) {
    return folderData[folderIndex].total;
  },

  getAllFoldersTotal() {
    let totalRecipes = 0;
    if (folderData) {
      if (folderData.length > 0) {
        folderData.forEach((folder) => {
          totalRecipes += folder.total;
        });
        return totalRecipes;
      } else {
        return 0;
      }
    }
  },

  getSubscriptionReceipt() {
    return userData.subscriptionReceipt;
  },

  setIsSubscribed(subscribedData) {
    isSubscribed = subscribedData;
  },

  getIsSubscribed() {
    return isSubscribed;
  }

}

