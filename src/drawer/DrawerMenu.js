import React from 'react';
import {NavigationActions} from 'react-navigation';
import { Text, View, TouchableOpacity, Image } from 'react-native';
import Styles from '../common/Styles';

class DrawerMenu extends React.Component {

  navigateToScreen = ( route ) => (
    () => {
    const navigateAction = NavigationActions.navigate({
      routeName: route
    });
    this.props.navigation.dispatch(navigateAction);
  });

  render() {
    return (
      <View style={Styles.drawerContainer}>

        <View style={Styles.drawerButtonContainer}>
          <TouchableOpacity onPress={() => {this.props.navigation.navigate('HomeStack')}}>
            <View style={Styles.drawerButton}>
              <Image style={Styles.drawerButtonIcon} source={require('../../img/icon-drawer-home.png')} />
              <Text style={Styles.drawerButtonText}>My Feed</Text>
            </View>
          </TouchableOpacity>
        </View>

        <View style={Styles.drawerButtonContainer}>
          <TouchableOpacity onPress={() => this.props.navigation.navigate('MyRecipesStack')}>
            <View style={Styles.drawerButton}>
              <Image style={Styles.drawerButtonIcon} source={require('../../img/icon-drawer-recipes.png')} />
              <Text style={Styles.drawerButtonText}>My Recipes</Text>
            </View>
          </TouchableOpacity>
        </View>        

        <View style={Styles.drawerButtonContainer}>
          <TouchableOpacity onPress={() => {this.props.navigation.navigate('CollectionsStack')}}>
            <View style={Styles.drawerButton}>
              <Image style={Styles.drawerButtonIcon} source={require('../../img/icon-drawer-collections.png')} />
              <Text style={Styles.drawerButtonText}>Featured Collections</Text>
            </View>
          </TouchableOpacity>
        </View>

        <View style={Styles.drawerButtonContainer}>
          <TouchableOpacity onPress={() => {this.props.navigation.navigate('LikedStack')}}>  
            <View style={Styles.drawerButton}>
              <Image style={Styles.drawerButtonIcon} source={require('../../img/icon-drawer-favourites.png')} />
              <Text style={Styles.drawerButtonText}>Liked</Text>
            </View>
          </TouchableOpacity>
        </View>

        <View style={Styles.drawerButtonContainer}>
          <TouchableOpacity onPress={() => {this.props.navigation.navigate('ShoppingListStack')}}>
            <View style={Styles.drawerButton}>
              <Image style={Styles.drawerButtonIcon} source={require('../../img/icon-drawer-shoppinglist.png')} />
              <Text style={Styles.drawerButtonText}>Shopping List</Text>
            </View>
          </TouchableOpacity>
        </View>

      </View>
    );
  }

}

export default DrawerMenu;
