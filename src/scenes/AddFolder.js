import React, {useState, useEffect} from 'react';
import { Text, TextInput, View, TouchableOpacity, Image, Dimensions } from 'react-native';
import { KeyboardAwareScrollView } from 'react-native-keyboard-aware-scroll-view'
import firestore from '@react-native-firebase/firestore';

import UserProxy from '../data/UserProxy';
import Styles from '../common/Styles';
import Colors from '../common/Colors';

const AddFolder = ({navigation, route}) => {

  const {title, rename, folderIndex} = route.params;
  const {width, height} = Dimensions.get('window');
  const [folderName, setFolderName] = useState('');
  const [nameError, setNameError] = useState('');
  const [loading, setLoading] = useState(false);
  const [edited, setEdited] = useState(false);

  let userFolders = UserProxy.getFolders();

  navigation.setOptions({
    headerLeft: () => (
      <TouchableOpacity
        style={{marginLeft:20}}
        onPress={() => navigation.navigate('MyRecipes')}

      >
        <Image source={require('../../img/icon-back-reverse.png')} />
      </TouchableOpacity>
    ),
    title: title
  });   
  
  onInputChange = (value) => {
    setEdited(true);
    setFolderName(value);
  };

  useEffect(() => {
    if (rename) {
      if (!edited) {
        setFolderName(userFolders[folderIndex].title);
      }
    }
  }),[rename, folderName]; 

  function handleSavePress() {
    const userRef = firestore().collection('users').doc(UserProxy.getUID());
    if (folderName === '') {
      setNameError('Please enter a folder name');
    } else {
      const folderExistsIndex = userFolders.indexOf(userFolders[folderIndex])
      if (rename && folderExistsIndex !== -1) {
        userFolders[folderIndex].title = folderName;
      } else {
        userFolders.push({title: folderName, total: 0})
      }      
      userRef.update({
        myFolders: userFolders
        //myFolders: rename ? userFolders : firestore.FieldValue.arrayUnion({title: folderName, total: 0})
      })
      .then (() => {
        console.log('folder added/renamed successfully')
        navigation.navigate('MyRecipes', {refreshMe: true});
      })
      .catch((error) => {
        console.log('error adding folder', error)
      })  
    }   
  }

  return (
    <KeyboardAwareScrollView contentContainerStyle={[Styles.loadingContainer, { backgroundColor:'#fff' }]}>
      <View style={{height:180, width: width - 20}}>
        <TextInput
          style={[Styles.textInput,
            { marginBottom: 20,
              paddingBottom: 10,
              fontSize: 22,
              fontWeight: 'bold',
              flex:1,
              borderBottomColor: nameError ? Colors.grey: '#000'}]}
          value={folderName}
          onChangeText={(inputText) => {
            onInputChange(inputText);
            setNameError('');
          }}
          returnKeyType={'next'}
          underlineColorAndroid='transparent'
          placeholder={'Folder name'} />  
        {nameError !== '' &&
          <View style={{flexDirection: 'row', alignItems: 'center', justifyContent: 'flex-end', marginBottom:10, marginTop: -10}}>
            <Text style={[Styles.smallText, {color: '#ff0000'}]}>{nameError}</Text>
            <View style={Styles.errorIcon}>
              <Text style={[Styles.smallText, {color: '#fff'}]}>!</Text>
            </View>
          </View>      
        }    
        <TouchableOpacity
          style={[Styles.button, {alignSelf:'stretch', justifyContent: 'center', opacity: loading ? 0.3 : 1, height: 50}]}
          disabled={loading ? true : false}
          onPress={() => handleSavePress()}>
          <Text style={Styles.buttonText}>Save</Text>
        </TouchableOpacity>     
        <TouchableOpacity
          onPress={() => navigation.goBack()}
          style={{alignSelf:'center', marginTop:20}}>
          <Text style={[Styles.defaultText, {fontSize:15, fontFamily: 'HelveticaNeue-Bold'}]}>Cancel</Text>
        </TouchableOpacity>   
      </View>         
    </KeyboardAwareScrollView>
  )  

}

export default AddFolder;                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                          