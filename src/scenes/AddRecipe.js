import React, {useEffect, useState} from 'react';
import { Text, View, Switch, TouchableOpacity, Image, ImageBackground, TextInput, Platform, NativeModules, Picker, Dimensions } from 'react-native';
import { KeyboardAwareScrollView } from 'react-native-keyboard-aware-scroll-view'
import { AutoGrowingTextInput } from 'react-native-autogrow-textinput';
import ImagePicker from 'react-native-image-picker';
import storage from '@react-native-firebase/storage';
import Firebase from '../communication/Firebase';
import Styles from '../common/Styles';
import Colors from '../common/Colors';
import UserProxy from '../data/UserProxy';
import DataConstants from '../data/DataConstants';
import { SafeAreaView } from 'react-native-safe-area-context';
//import RecipeStorage from '../storage/RecipeStorage';

const AddRecipe = ({route, navigation}) => {

  const {edit, 
    recipeData, 
    recipeID, 
    updatedTagsChosen, 
    updatedFolderChosen, 
    folderEdit, 
    isSharedEdit,
    isPremium
  } = route.params;
  const tagsAll = DataConstants.getTags();
  const {width} = Dimensions.get('window');

  const [loading, setLoading] = useState(false);
  const [loadingMessage, setLoadingMessage] = useState('Loading recipes...');
  const [imageData, setImgData] = useState(null);
  const [recipe, setRecipe] = useState({tags:[], ingredients:[{ingredient:'', quantity:''}]});
  const [titleError, setTitleError] = useState(''); 
  const [ingredientsError, setIngredientsError] = useState('');
  const [methodError, setMethodError] = useState('');
  const [uploadImageFlag, setUploadImageFlag] = useState(false);
  const [cookingHours, setHours] = useState(0);
  const [cookingMinutes, setMinutes] = useState(0);
  const [folderChosen, setFolderChosen] = useState(0);
  const [originalFolder, setOriginalFolder] = useState(undefined);
  const [isShared, setIsShared] = useState(false);
  const [edited, setEdited] = useState(false);
  const [cloudTextReturned, setCloudTextReturned] = useState(false);
  const [cloudTextResults, setCloudTextResults] = useState('');
  const [cloudTextType, setCloudTextType] = useState('');
  const isSubscriber = UserProxy.getIsSubscribed();

  useEffect(() => {
    if (route.params?.edit) {
      if (!edited) {
        setFolderChosen(folderEdit);
        setIsShared(isSharedEdit);
        setRecipe(recipeData);
        setHours(recipeData.cookingHours);
        setMinutes(recipeData.cookingMinutes);
        if (recipeData.hasImage) {
          getAndLoadHttpUrl();
        }
        setOriginalFolder(folderEdit)
        setEdited(true);
      }
    }
  //}),[recipeData, isSharedEdit, folderEdit, edited];  
  }),[route.params?.edit];  

  // useEffect(() => {
  //   const unsubscribe = navigation.addListener('focus', () => {
  //     if (updatedFolderChosen) {
  //       setFolderChosen(updatedFolderChosen);
  //       setEdited(true);
  //     }      
  //     if (updatedTagsChosen) {
  //       // console.log(updatedTagsChosen);
  //       setRecipe( {...recipe, tags: updatedTagsChosen} );        
  //     }
  //   });
  //   return unsubscribe;
  // }, [navigation, route]); 

  useEffect(() => {
    if (route.params?.updatedTagsChosen) {
      console.log('they are updated');
    }
    if (route.params?.updatedFolderChosen) {
      setFolderChosen(route.params?.updatedFolderChosen);
    }    
  }, [route.params?.updatedTagsChosen, route.params?.updatedFolderChosen]);

  async function getAndLoadHttpUrl() {
    const ref = storage().ref(`recipe-images/${recipeID}.jpg`);
    ref.getDownloadURL().then(data => {
      //console.log(data);
      setImgData({url: data});
      //this.setState({ loadingImageData: false });
    }).catch(error => {
      //this.setState({ loadingImageData: false });
    })
  };

  function getErrorContainer(errorMsg) {
    if (errorMsg) {
      return (
        <View style={{flexDirection: 'row', alignItems: 'center', justifyContent: 'flex-end', marginBottom:10, marginTop: -10}}>
          <Text style={[Styles.smallText, {color: '#ff0000'}]}>{errorMsg}</Text>
          <View style={Styles.errorIcon}>
            <Text style={[Styles.smallText, {color: '#fff'}]}>!</Text>
          </View>
        </View>
      )
    }
  };

  function selectPhotoTapped() {
    const options = {
      title: 'Photo Picker',
      takePhotoButtonTitle: 'Take Photo...',
      chooseFromLibraryButtonTitle: 'Choose from Library...',
      quality: 0.8,
      maxWidth: 300,
      maxHeight: 300,
      allowsEditing: false,
      storageOptions: {
        skipBackup: true
      }
    };

    ImagePicker.showImagePicker(options, (response) => {

      if (response.didCancel) {
        console.log('User cancelled photo picker');
      }
      else if (response.error) {
        console.log('ImagePicker Error: ', response.error);
      }
      else if (response.customButton) {
        console.log('User tapped custom button: ', response.customButton);
      }
      else {
        var source = {uri: response.uri};
        setUploadImageFlag(true);
        setImgData(source)
      }
    });
  };

  function handleScanPress(type) {
    setCloudTextType(type);
    //console.log('scan pressed')
    const options = {
      title: 'Photo Picker',
      takePhotoButtonTitle: 'Take Photo...',
      chooseFromLibraryButtonTitle: 'Choose from Library...',
      quality: 0.8,
      maxWidth: isSubscriber ? 600 : 300,
      maxHeight: isSubscriber ? 600 : 300,
      allowsEditing: false,
      storageOptions: {
        skipBackup: true
      }
    };

    ImagePicker.showImagePicker(options, (response) => {

      // console.log('Response = ', response);

      if (response.didCancel) {
        console.log('User cancelled photo picker');
      }
      else if (response.error) {
        console.log('ImagePicker Error: ', response.error);
      }
      else if (response.customButton) {
        console.log('User tapped custom button: ', response.customButton);
      }
      else {
        var source;

        // You can display the image using either:
        //source = {uri: 'data:image/jpeg;base64,' + response.data, isStatic: true};

        // Or:
        if (Platform.OS === 'android') {
           source = {uri: response.uri, isStatic: true};
        } else {
           source = {uri: response.uri.replace('file://', ''), isStatic: true};
        }

        var imgPath = source.uri;

        var TextDetector = NativeModules.TextDetector;
        TextDetector.detect(imgPath, (error, resultText) => {
      	  if (error) {
      	    console.error(error);
      	  } else {
            //console.log(resultText);
            setCloudTextReturned(true);
            setCloudTextResults(resultText);
      	  }
        });
      }
    });
  };

  function handleCloudTextAddPress() {
    switch(cloudTextType) {
      case 'title' : {
        setRecipe({...recipe, title: cloudTextResults})
        setEdited(true);
        setTitleError('');
        setCloudTextReturned(false);   
      }
      case 'ingredient' : {      
        newIngredient = {ingredient: cloudTextResults, quantity: ''};
        var tmpRecipeArray = recipe.ingredients;
        tmpRecipeArray.push(newIngredient);
        // console.log(tmpRecipeArray)
        setRecipe({...recipe, ingredients: tmpRecipeArray.length > 1 ? tmpRecipeArray : newIngredient});
        // console.log(recipe)
        setEdited(true);
        setIngredientsError('');
        setCloudTextReturned(false);  
      }
      case 'method' : {
        setRecipe({...recipe, method: cloudTextResults})
        setEdited(true);
        setMethodError('');
        setCloudTextReturned(false);           
      }
      default : {
        setCloudTextReturned(false); 
      }
    }
  };

  function handleNewIngredientPress() {
    recipe.ingredients.push({quantity:'', ingredient:''});
    const tmpRecipeData = {...recipe}
    setRecipe(tmpRecipeData);
  };

  function handleSavePress() {
    var titleValid = recipe.title !== undefined ? true : false;
    var ingredientsValid = recipe.ingredients.length >= 1 ? true : false;
    var methodValid = recipe.method !== undefined ? true : false;
    setTitleError(titleValid ? null : 'Please enter a title');
    setIngredientsError(ingredientsValid ? null : 'ingredients');
    setMethodError(methodValid ? null : 'Please enter a method');

    if (titleValid && ingredientsValid && methodValid) {
      setLoading(true);
      setLoadingMessage('Saving recipe...');
      //console.log(recipe);
      saveData();
    }
  };

  async function saveData() {

    setLoading(true);      

    recipe.hasImage = imageData ? true : false;
    recipe.cookingHours = cookingHours;
    recipe.cookingMinutes = cookingMinutes;
    recipe.date = Math.floor(Date.now());
    recipe.isShared = isShared;

    // console.log(recipe);

    if (edit) {
      Firebase.editMyRecipes(recipeID, recipe, folderChosen, isShared)
      .then((response) => {
        if (response) {
          if (imageData && uploadImageFlag) {
            Firebase.uploadImage(recipeID, imageData)
            .then((response) => {
              if (response) {
                if (originalFolder !== folderChosen) {
                  Firebase.updateFolders(folderChosen, originalFolder)
                  .then((response) => {
                    if (isShared) {
                      Firebase.shareRecipe(recipeID, recipe)
                      .then((response) => {
                        if (response) {
                          goBackAndRefresh();
                        }
                      })
                    } else {
                      Firebase.unShareRecipe(recipeID)
                      .then((response) => {
                        if (response) {
                          goBackAndRefresh();
                        }
                      })
                    }
                  })
                } else {
                  if (isShared) {
                    Firebase.shareRecipe(recipeID, recipe)
                    .then((response) => {
                      if (response) {
                        goBackAndRefresh();
                      }
                    })
                  } else {
                    Firebase.unShareRecipe(recipeID)
                    .then((response) => {
                      if (response) {
                        goBackAndRefresh();
                      }
                    })
                  }                  
                }
              }
            })
          } else {
            if (originalFolder !== folderChosen) {
              Firebase.updateFolders(folderChosen, originalFolder)
              .then((response) => {
                if (isShared) {
                  Firebase.shareRecipe(recipeID, recipe)
                  .then((response) => {
                    if (response) {
                      goBackAndRefresh();
                    }
                  })
                } else {
                  Firebase.unShareRecipe(recipeID)
                  .then((response) => {
                    if (response) {
                      goBackAndRefresh();
                    }
                  })
                }                
              })
            } else {
              if (isShared) {
                Firebase.shareRecipe(recipeID, recipe)
                .then((response) => {
                  if (response) {
                    goBackAndRefresh();
                  }
                })
              } else {
                Firebase.unShareRecipe(recipeID)
                .then((response) => {
                  if (response) {
                    goBackAndRefresh();
                  }
                })
              }              
            }
          }
        }
      })
    } else {
      Firebase.addToMyRecipes(recipe, folderChosen, isShared)
      .then((docRef) => {
        if (imageData && uploadImageFlag) {
          Firebase.uploadImage(docRef, imageData)
          .then((response) => {
            Firebase.updateFolders(folderChosen)
            .then((response) => {
              if (response) {
                if (isShared) {
                  Firebase.shareRecipe(docRef, recipe)
                  .then((response) => {
                    if (response) {
                      goBackAndRefresh();
                    }
                  })
                } else {
                  Firebase.unShareRecipe(docRef)
                  .then((response) => {
                    if (response) {
                      goBackAndRefresh();
                    }
                  })
                }
              }
            })                    
          })
        } else {
          Firebase.updateFolders(folderChosen)
          .then((response) => {
            if (response) {
              if (isShared) {
                Firebase.shareRecipe(docRef, recipe)
                .then((response) => {
                  if (response) {
                    goBackAndRefresh();
                  }
                })
              } else {
                Firebase.unShareRecipe(docRef)
                .then((response) => {
                  if (response) {
                    goBackAndRefresh();
                  }
                })
              }
            }
          })        
        }
      }); 
    }   
  };

  function deleteRecipe() {
    if (edit) {
      Firebase.deleteRecipe(recipeID)
      .then((response) => {
        if (response) {
          if (recipe.hasImage) {
            Firebase.deleteImage(recipeID)
            .then((response) => {
              if (response) {
                Firebase.updateFoldersDeleteRecipe(folderChosen)
                .then((response) => response && goBackAndRefresh())
              }
            })
          } else {
            Firebase.updateFoldersDeleteRecipe(folderChosen)
            .then((response) => response && goBackAndRefresh())            
          }
        }
      })
    } else {
      navigation.goBack();
    }
  };

  function goBackAndRefresh() {
    navigation.navigate('MyRecipes', {refreshMe:true})
  };

  if (loading) {
    return (
      <View style={Styles.loadingContainer}>
        <Image source={require('../../img/icon-drawer-recipes.png')} />
        <Text style={[Styles.defaultTitle, {marginTop: 20}]}>{loadingMessage}</Text>
        {/* <TouchableOpacity
          style={{position:'absolute', bottom: 20, height: 40, flex: 1}}
          onPress={() => this.setState({loading: false})}>
          <Text style={{flex:1, textAlign: 'center'}}>Cancel</Text>
        </TouchableOpacity> 
        */}
      </View>
    )
  } else if (cloudTextReturned) {
    return (
      <SafeAreaView style={Styles.loadingContainer}>
        <KeyboardAwareScrollView style={{paddingTop: 20, width: width - 40}}>

        <Text style={[Styles.defaultMainTitle, 
          { marginBottom:10, 
            color: Colors.darkBlue,
            flex:1,
            textAlign: 'center'
          }
        ]}>Use This Text?</Text>
        <AutoGrowingTextInput
          style={[Styles.textInput,
            { 
              marginBottom: 20,
              paddingBottom: 10,
              borderBottomColor: '#fff',
              fontFamily: 'HelveticaNeue-Medium'
            }
          ]}
          value={cloudTextResults}
          onChangeText={(text) => {
            setCloudTextResults(text)
          }}
          underlineColorAndroid='transparent'/>  

        <TouchableOpacity
          style={[Styles.button, {alignSelf:'stretch', justifyContent: 'center'}]}
          onPress={() => handleCloudTextAddPress()}
          >
          <Text style={Styles.buttonText}>Add text</Text>
        </TouchableOpacity>

        <TouchableOpacity
          onPress={() => setCloudTextReturned(false)}>
          <Text style={{textAlign: 'center', marginTop: 20}}>Cancel</Text>
        </TouchableOpacity>

        </KeyboardAwareScrollView>
      </SafeAreaView>
    )    
  } else {
    return (
      <View style={{flex:1}}>
        <KeyboardAwareScrollView style={{flex:1, backgroundColor:Colors.brightGreen}}>
          <TouchableOpacity onPress={() => selectPhotoTapped()}>
            <ImageBackground source={imageData} style={Styles.mainImageRecipe}>
              {imageData == null ? <Image source={require('../../img/icon-img.png')} /> : null}
            </ImageBackground>
          </TouchableOpacity>

          <View style={[Styles.paddedContainer, {backgroundColor: '#FFF'}]}>

            <View style={{flexDirection: 'row', flex: 1}}>
              <TextInput
                style={[Styles.textInput,
                  { marginBottom: 20,
                    paddingBottom: 10,
                    fontSize: 22,
                    //fontWeight: 'bold',
                    flex:1,
                    borderBottomColor: titleError ? Colors.grey: '#000'}]}
                value={recipe.title}
                onChangeText={(text) => {
                  setRecipe({...recipe, title: text})
                  setEdited(true);
                  setTitleError('')
                }}
                returnKeyType={'next'}
                underlineColorAndroid='transparent'
                placeholderTextColor={'#b9b9b9'}
                placeholder={'Add a title'} />
                <TouchableOpacity
                  style={{alignItems: 'flex-end'}}
                  onPress={() => handleScanPress('title')}
                  >
                  <Image source={require('../../img/icon-camera.png')} style={{tintColor: Colors.darkBlue}} />
                </TouchableOpacity>                
            </View>                  

            {getErrorContainer(titleError)}

            <View style={{height: 40}}>
              <TouchableOpacity 
                onPress={() => {
                  navigation.navigate('ChooseTags', {tagsChosen: recipe.tags}); 
                }}
              >
                <View style={{flexDirection: 'row', alignItems: 'center'}}>

                  {recipe.tags && recipe.tags.length > 0 ? 
                    recipe.tags.map(function(tag, i) {
                      return (
                        <Image source={tagsAll[tag].icon} style={{marginRight: 10}} key={i} />
                    )}) :
                    <Text style={[Styles.defaultTitle,{flex:1}]}>Tags</Text> 
                  }

                  <Image source={require('../../img/icon-forward-small.png')} />
                </View>
              </TouchableOpacity>
            </View>

            <View style={{flexDirection: 'row', height: 40, alignItems: 'center'}}>
              <Text style={[Styles.defaultTitle, {flex:1}]}>Ingredients</Text>
              <TouchableOpacity onPress={() => handleScanPress('ingredient')}>
                <Image source={require('../../img/icon-camera.png')} style={{tintColor: Colors.darkBlue}} />
              </TouchableOpacity>                 
            </View>

            {recipe.ingredients && recipe.ingredients.length > 0 && 
              recipe.ingredients.map(function(ingredient, i) {
                return (
                  <View style={{flexDirection: 'row'}} key={i}>
                    <TextInput
                      style={[Styles.textInput, Styles.textInputQuantity, {borderBottomColor: ingredientsError ? Colors.grey: '#000'}]}
                      // keyboardType={'numeric'}
                      value={ingredient.quantity}
                      placeholder={'1'}
                      onChangeText={(text) => {
                        recipe.ingredients[i].quantity = text;
                        const tmpRecipeData = {...recipe}
                        setRecipe(tmpRecipeData);
                        setIngredientsError('');
                        setEdited(true);
                      }}
                      returnKeyType={'next'}
                      placeholderTextColor={'#b9b9b9'}
                      underlineColorAndroid='transparent'
                    />
                  <TextInput
                    style={[Styles.textInput, Styles.textInputIngredient, {borderBottomColor: ingredientsError ? Colors.grey: '#000'}]}
                    value={ingredient.ingredient}
                    placeholder={'Add an ingredient'}
                    onChangeText={(text) => {
                      recipe.ingredients[i].ingredient = text;
                      const tmpRecipeData = {...recipe}
                      setRecipe(tmpRecipeData);        
                      setIngredientsError('');
                      setEdited(true);
                    }}
                    returnKeyType={'next'}
                    onKeyPress={(keyPress) => checkIngredientForDelete(keyPress.nativeEvent.key, i)}
                    placeholderTextColor={'#b9b9b9'}
                    underlineColorAndroid='transparent'
                    />
                  </View>
                )
              })
            }
            {getErrorContainer(ingredientsError)}  
      

            <TouchableOpacity
              style={[Styles.buttonRound, {marginTop: 10, marginBottom:20, alignSelf: 'flex-end'}]}
              onPress={() => handleNewIngredientPress()}>
              <Image source={require('../../img/icon-add-ingredient.png')} />
            </TouchableOpacity>

            <View style={{height: 40, flexDirection: 'row'}}>
              <Text style={[Styles.defaultTitle, {flex:1}]}>Method</Text>
              <TouchableOpacity onPress={() => handleScanPress('method')}>
                <Image source={require('../../img/icon-camera.png')} style={{tintColor: Colors.darkBlue}} />
              </TouchableOpacity>                
            </View>
            <AutoGrowingTextInput
              style={[Styles.textInput,
                { 
                  marginBottom: 20,
                  paddingBottom: 10,
                  borderBottomColor: methodError ? Colors.grey: '#000',
                  fontFamily: 'HelveticaNeue-Medium'
                }
              ]}
              value={recipe.method}
              onChangeText={(text) => {
                recipe.method = text;
                const tmpRecipeData = {...recipe}
                setRecipe(tmpRecipeData);                     
                setMethodError('');
                setEdited(true);
              }}
              underlineColorAndroid='transparent'
              placeholderTextColor={'#b9b9b9'}
              placeholder={'Enter method'} />
              {getErrorContainer(methodError)}

            <View style={{flexDirection: 'row', height: 40, alignItems: 'center'}}>
              <Text style={Styles.defaultTitle}>Cooking time</Text>
            </View>

            <View style={{height: 200, marginBottom: 20}}>
                <View style={{flexDirection: 'row', alignItems: 'center'}}>

                  {/* {recipe.cookingTime !== "" ?
                    <Text style={[Styles.defaultText,{flex:1, fontSize: 15}]}>{recipe.cookingTime}</Text>
                    :
                    <Text style={[Styles.defaultText,{flex:1, fontSize: 15, color:Colors.grey}]}>Choose</Text>
                  }  */}

                  <Picker
                    selectedValue={cookingHours}
                    style={{height: 50, width: 100, marginRight: 5}}
                    onValueChange={(itemValue, itemIndex) =>
                      setHours(itemValue)
                    }>
                    <Picker.Item label="0h" value={0} />  
                    <Picker.Item label="1h" value={1} />
                    <Picker.Item label="2h" value={2} />
                    <Picker.Item label="3h" value={3} />
                    <Picker.Item label="4h" value={4} />
                    <Picker.Item label="5h" value={5} />
                    <Picker.Item label="6h" value={6} />
                    <Picker.Item label="7h" value={7} />
                    <Picker.Item label="8h" value={8} />
                    <Picker.Item label="9h" value={9} />      
                  </Picker>      

                  <Picker
                    selectedValue={cookingMinutes}
                    style={{height: 50, width: 100}}
                    onValueChange={(itemValue, itemIndex) =>
                      setMinutes(itemValue)
                    }>
                    <Picker.Item label="00m" value={0} />  
                    <Picker.Item label="05m" value={5} />
                    <Picker.Item label="10m" value={10} />
                    <Picker.Item label="15m" value={15} />
                    <Picker.Item label="20m" value={20} />
                    <Picker.Item label="25m" value={25} />
                    <Picker.Item label="30m" value={30} />
                    <Picker.Item label="35m" value={35} />
                    <Picker.Item label="40m" value={40} />
                    <Picker.Item label="45m" value={45} />
                    <Picker.Item label="50m" value={40} />
                    <Picker.Item label="55m" value={55} />
                  </Picker>              

                </View>
            </View>        

            <View style={{height: 50, marginBottom: 20}}>
              <Text style={[Styles.defaultTitle, {flex:1}]}>Folder</Text>
              <TouchableOpacity 
                onPress={() => navigation.navigate('ChooseFolder', {initialFolder: folderChosen})}
              >
                <View style={{flexDirection: 'row', marginTop: 10}}>
                  <View style={{flexDirection: 'row', alignItems: 'center', flex: 1}}>
                    <View style={[Styles.folderIcon, {backgroundColor:DataConstants.getFolderColors()[folderChosen].color}]} />
                    <Text style={Styles.defaultText}>{UserProxy.getFolders()[folderChosen].title.toUpperCase()}</Text>
                  </View>
                  <Image source={require('../../img/icon-forward-small.png')} />    
                </View>
              </TouchableOpacity>
            </View>                

            {!isPremium &&
              <View style={{flexDirection: 'row', height: 40, marginBottom: 20}}>
                {UserProxy.getScreenName() ? (
                  <Text style={[Styles.defaultTitle,{flex:1, marginTop: 6}]}>
                    Share
                  </Text>
                )
                : 
                (
                  <TouchableOpacity 
                    style={{flex:1, marginTop: 6}}
                    onPress={() => navigation.navigate('Profile')}>
                    <Text style={Styles.defaultTitle}>
                      { UserProxy.getIsAnonymous() ? 'Register to share' : 'Add a screen name to share' }
                    </Text>
                  </TouchableOpacity>
                )
                }
                <Switch
                  disabled={UserProxy.getScreenName() == null ? true : false}
                  onValueChange={(value) => {setIsShared(value)}}
                  value={isShared}
                />
              </View>              
            }
            
            <TouchableOpacity
              style={[Styles.button, {alignSelf:'stretch', justifyContent: 'center', opacity: loading ? 0.3 : 1}]}
              disabled={loading ? true : false}
              onPress={() => handleSavePress()}>
              <Text style={Styles.buttonText}>Save</Text>
            </TouchableOpacity>

            {edit ? 
              <TouchableOpacity
                onPress={() => deleteRecipe()}
                style={{alignSelf:'center', marginTop:20}}>
                <Text style={[Styles.defaultText, {fontSize:15, fontFamily: 'HelveticaNeue-Bold'}]}>Delete</Text>
              </TouchableOpacity>        
              :
              <TouchableOpacity
                onPress={() => navigation.goBack()}
                style={{alignSelf:'center', marginTop:20}}>
                <Text style={[Styles.defaultText, {fontSize:15, fontFamily: 'HelveticaNeue-Bold'}]}>Cancel</Text>
              </TouchableOpacity>          
            }          

          </View>
          <View style={Styles.viewRecipeHeader}>
            <TouchableOpacity onPress={() => navigation.navigate('MyRecipes')}>
              <Image source={require('../../img/icon-back-reverse.png')} />
            </TouchableOpacity>
          </View>
        </KeyboardAwareScrollView>
      </View>
    )
  };

  function checkIngredientForDelete(key, i) {
    if (key === 'Backspace') {
      if (recipe.ingredients[i].ingredient.length < 1) {
        recipe.ingredients.splice(i, 1);
        const tmpRecipeData = {...recipe}
        setRecipe(tmpRecipeData);   
      }
    }
  }

};

export default AddRecipe;
