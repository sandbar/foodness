import React, {useEffect, useState} from 'react';
import { Text, View, Image, TouchableOpacity, Dimensions, Animated } from 'react-native';

import { SwipeListView } from 'react-native-swipe-list-view';

import ShoppingListStorage from '../storage/ShoppingListStorage';
import Styles from '../common/Styles';
import Colors from '../common/Colors';

let rowTranslateAnimatedValues = [];

const ShoppingList = ({navigation}) => {

  const {width} = Dimensions.get('window');
  const [shoppingListData, setShoppingListData] = useState([]);
  const [animating, setAnimating] = useState(false);

  useEffect(() => {
    const unsubscribe = navigation.addListener('focus', () => {
      console.log('focussed, get data');
      getData();
    });
    return unsubscribe;
  }, [navigation]);   
  
  const getData = async () => {
    const data = await ShoppingListStorage.getShoppingList();
    if (data) {
      // console.log(data);
      for (var i = 0; i < data.length; i++) {
        rowTranslateAnimatedValues.push(new Animated.Value(1));
      }
      setShoppingListData(data);
      setAnimating(false);
    }
  }

  const clearShoppingList = async () => {
   try {
    await ShoppingListStorage.clearShoppingList();
   } catch(e) {
     console.log(e);
   } 
   setShoppingListData([]);
  };

  if (shoppingListData.length > 0) {
    return (
      <View style={[Styles.containerGeneric, {padding:0}]}>
        <SwipeListView
          data={shoppingListData}
          keyExtractor={(item, index) => index.toString()}
          useNativeDriver={false}
          renderItem={ ({item, index}) => (
            <Animated.View 
              style={[Styles.ingredientRow, 
                {width: width, 
                backgroundColor: index % 2 == 0 ? Colors.lightestGrey : Colors.altLightestGrey,
                height: rowTranslateAnimatedValues[index].interpolate({
                  inputRange: [0, 1],
                  outputRange: [0, 50],
                }),
                paddingVertical: rowTranslateAnimatedValues[index].interpolate({
                  inputRange: [0, 1],
                  outputRange: [0, 15],
                }),                
                }
              ]} >
              <Text style={[Styles.defaultText, {color:'#000', width:80, marginLeft:20}]}>{item.quantity}</Text>
              <Text style={[Styles.defaultText, {color:'#000'}]}>{item.ingredient}</Text>
            </Animated.View>              
          )}
          renderHiddenItem={ ({item, index}) => (
            <View style={Styles.hiddenRow}>
              <Text style={[Styles.defaultText, {color: '#fff'}]}>Delete</Text>
            </View>
          )}
          disableRightSwipe
          rightOpenValue={-width}
          onSwipeValueChange={onSwipeValueChange}
          ListFooterComponent={() => (
            <TouchableOpacity
              style={{padding: 10, alignSelf: 'flex-end'}}
              onPress={() => clearShoppingList()}>
              <Image source={require('../../img/icon-bin.png')}  />
            </TouchableOpacity>            
          )}
        />
      </View>   
    )
  } else {
    return (
      <View style={[Styles.containerGeneric, {justifyContent:'center'}]}>
        <Image source={require('../../img/icon-drawer-shoppinglist.png')} style={{marginBottom: 20}} />
        <Text style={Styles.defaultText}>No items on your shopping list yet</Text>
      </View>
    )
  }

  function onSwipeValueChange(swipeData) {
      const { key, value } = swipeData;
      if (value < -width && !animating) {
        setAnimating(true);
        Animated.timing(rowTranslateAnimatedValues[key], {
          toValue: 0,
          duration: 200,
          useNativeDriver: false,
        }).start(() => {
          setShoppingListData([]);
          removefromShoppingList(key);
        });
      }
  };

  async function removefromShoppingList(prevIndex) {
    try {
      await ShoppingListStorage.removefromShoppingList(prevIndex);
    } catch(e) {
      console.log(e);     
    }
    getData();    
  };  

}

export default ShoppingList;