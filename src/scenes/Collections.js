import React, { useState, useEffect, useCallback } from 'react';
import { Text, View, FlatList } from 'react-native';
import firestore from '@react-native-firebase/firestore';
import CollectionListItem from '../components/CollectionListItem';
import Colors from '../common/Colors';
import Styles from '../common/Styles';

const Collections = ({navigation}) => {
 
  const [lastDoc, setLastDoc] = useState({});
  const [tmpLastDoc, setTmpLastDoc] = useState({});    
  const [collections, setCollections] = useState([]);  

  useEffect(() => {
    let isSubscribed = true;
    firestore().collection('recipeCollections')
    .orderBy('date', 'desc')
    .startAfter(lastDoc)
    .limit(10)      
    .get()
    .then((querySnapshot) => {
      let listItems = [];
      querySnapshot.forEach((doc) => {
        if (isSubscribed) {
          //console.log(doc.id, '=>', doc.data());
          const { tags, title, hasImage, published, description } = doc.data();
          published &&
            listItems.push({
              key: doc.id,
              tags: tags,
              title, title,
              hasImage: hasImage,
              description: description,
              published: published
            });
        }
      });
      setCollections(listItems);
      var lastVisible = querySnapshot.docs[querySnapshot.docs.length-1];
      if (lastVisible) {
        setTmpLastDoc(lastVisible);
      }       
    });
    return () => isSubscribed = false;      
  },[]);    

  const handleOnEndReached = useCallback(() => {
    setLastDoc(tmpLastDoc);
  });   

  if (collections.length > 0) {
    return (
      <View style={{backgroundColor: Colors.slate, flex:1}}>
        <FlatList
          data={collections}
          contentContainerStyle = {Styles.collectionsList}
          onEndReachedThreshold={0.1}
          onEndReached={() => handleOnEndReached()}                   
          renderItem = {({item, index}) => 
            <CollectionListItem 
              item={item} 
              navigation={navigation} 
              key={index.toString()} 
              index={index} />}
        />
      </View>
    )
  } else {
    return (
      <View style={[Styles.containerGeneric, {justifyContent:'center'}]}>
        <Text style={Styles.defaultText}>No collections yet</Text>
      </View>
    )
  }  

}

export default Collections;