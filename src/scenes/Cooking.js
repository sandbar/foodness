import React, { useState } from 'react';
import { View, Image, TouchableOpacity, Text } from 'react-native';
import PushNotificationIOS from "@react-native-community/push-notification-ios";
import { Timer } from 'react-native-stopwatch-timer'

import Styles from '../common/Styles';
import Colors from '../common/Colors';

const Cooking = ({navigation, route}) => {

  const {cookingTime} = route.params;
  const [timerStart, setTimerStart] = useState(true);

  const options = {
    container: {
      backgroundColor: Colors.darkBlue,
      padding: 5,
      borderRadius: 5,
      width: 300,
    },
    text: {
      fontSize: 64,
      color: '#FFF',
      marginLeft: 10,
    }
  };

  const handleTimerComplete = () => {
    //console.log('complete');
    handleCancel();
  };

  const handleCancel = () => {
    setTimerStart(false);
    PushNotificationIOS.cancelLocalNotifications();
    navigation.goBack();
  }

  return (
    <View style={Styles.loadingContainer}>
      <Image source={require('../../img/icon-drawer-recipes.png')} style={{marginBottom:20}} />
      <Timer totalDuration={cookingTime} start={timerStart}
        options={options}
        handleFinish={handleTimerComplete}
      />   
      <TouchableOpacity 
        style={{marginTop: 20}}
        onPress={() => {handleCancel()}}>
        <Text>Cancel</Text>
      </TouchableOpacity>   
    </View>
  );

}

export default Cooking;