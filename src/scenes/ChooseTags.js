import React, {useState, useEffect} from 'react';
import { Text, View, TouchableOpacity, Image, FlatList } from 'react-native';

import DataConstants from '../data/DataConstants';
import Styles from '../common/Styles';

const ChooseTags = ({navigation, route}) => {

  let {tagsChosen} = route.params;
  const newTagsChosen = tagsChosen;
  const tagsAll = DataConstants.getTags();
  const [refresh, setRefresh] = useState(false);

  navigation.setOptions({
    headerLeft: () => (
      <TouchableOpacity
        style={{marginLeft:20}}
        //onPress={() => navigation.navigate('AddRecipe',{updatedTagsChosen:newTagsChosen})}
        onPress={() => {
          tagsChosen = null;
          navigation.navigate('AddRecipe', {tags:newTagsChosen});
        }}
        //onPress={() => navigation.goBack()}
      >
        <Image source={require('../../img/icon-back-reverse.png')} />
      </TouchableOpacity>
    ),
  });    
  
  // useEffect(() => {
  //   const unsubscribe = navigation.addListener('blur', () => {
  //     console.log('blur')
  //     // navigation.setParams({
  //     //   updatedTagsChosen: tagsChosen
  //     // })      
  //   });
  //   return unsubscribe;
  // }, [navigation]);  

  function handleTagPress(tagID) {
    if (newTagsChosen.indexOf(tagsAll[tagID].id) < 0) {
      // does not exist, add
      newTagsChosen.push(tagID);
      // sort by ID
      newTagsChosen.sort(function(a,b){
        return a - b;
      });
    } else {
      // it exists, remove
      newTagsChosen.splice(newTagsChosen.indexOf(tagsAll[tagID].id), 1);
    }
  }

  return (
    <View style={{ backgroundColor:'#fff', flex:1 }}>
      <FlatList
        data={tagsAll}
        ItemSeparatorComponent={({highlighted}) => (
          <View style={[Styles.listSeparator]} />
        )}
        keyExtractor={(item, index) => item.id.toString()}
        //contentContainerStyle = {Styles.recipeList}
        extraData={refresh}
        renderItem={({item}) => (
          <TouchableOpacity
            onPress={() => {handleTagPress(item.id); setRefresh({refresh: !refresh})}}
            key={item.id}
            style={{flex:1, paddingHorizontal: 15, paddingVertical: 20, flexDirection: 'row', alignItems: 'center'}}>
            <Image source={tagsAll[item.id].icon} style={{marginRight: 10}} />
            <Text style={[Styles.defaultText, {flex:1}]}>{tagsAll[item.id].displayText}</Text>
            <Image source={require('./../../img/icon-tick.png')}
              style={{opacity: newTagsChosen.indexOf(tagsAll[item.id].id) > -1 ? 1 : 0}} />
          </TouchableOpacity>
        )}
      />
    </View>
  )  

}

export default ChooseTags;