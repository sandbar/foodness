import React, {useState, useEffect} from 'react';
import { Text, View, TouchableOpacity, Image, FlatList } from 'react-native';

import DataConstants from '../data/DataConstants';
import UserProxy from '../data/UserProxy';
import Styles from '../common/Styles';

const ChooseFolder = ({navigation, route}) => {

  const {initialFolder} = route.params;
  const userFolders = UserProxy.getFolders();
  const folderColors = DataConstants.getFolderColors(); 
  const [edited, setEdited] = useState(false)
  const [folderChosen, setFolderChosen] = useState(initialFolder);

  navigation.setOptions({
    headerLeft: () => (
      <TouchableOpacity
        style={{marginLeft:20}}
        onPress={() => {navigation.navigate('AddRecipe',{updatedFolderChosen:folderChosen}); console.log(folderChosen)}}
      >
        <Image source={require('../../img/icon-back-reverse.png')} />
      </TouchableOpacity>
    ),
  });   
  
  useEffect(() => {
    if (!edited) {
      setFolderChosen(initialFolder);
    }
  }),[folderChosen];

  function handleFolderPress(folderID) {
    setEdited(true);
    if (folderChosen !== folderID)
      setFolderChosen(folderID);
  }

  return (
    <View style={{ backgroundColor:'#fff', flex:1 }}>
      <FlatList
        data={userFolders}
        ItemSeparatorComponent={({highlighted}) => (
          <View style={[Styles.listSeparator]} />
        )}
        keyExtractor={(item, index) => index.toString()}
        renderItem={({item, index}) => (
          <TouchableOpacity
            onPress={() => {handleFolderPress(index)}}
            key={userFolders.indexOf(item)}
            style={{flex:1, paddingHorizontal: 15, paddingVertical: 20, flexDirection: 'row', alignItems: 'center'}}>
            <View style={[Styles.folderIcon, {backgroundColor:folderColors[index].color}]} />
            <Text style={[Styles.defaultText, {flex:1}]}>{item.title.toUpperCase()}</Text>
            <Image source={require('./../../img/icon-tick.png')}
              style={{opacity: userFolders.indexOf(item) === folderChosen ? 1 : 0}} />
          </TouchableOpacity>
        )}
      />
    </View>
  )  

}

export default ChooseFolder;                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                          