import React, { useState, useEffect, useCallback } from 'react';
import { Text, View, FlatList, Dimensions } from 'react-native';
import firestore from '@react-native-firebase/firestore';

import Styles from '../common/Styles';
import Colors from '../common/Colors';
import UserProxy from '../data/UserProxy';
import RecipeListItem from '../components/RecipeListItem';

const Home = ({navigation}) => {

  const {width, height} = Dimensions.get('window');
  const isPad = height/width < 1.5 ? true : false;
  const [recipes, setRecipes] = useState([]);  
  const [lastDoc, setLastDoc] = useState({});
  const [tmpLastDoc, setTmpLastDoc] = useState({});  
  const [refreshData, setRefreshData] = useState(false);
  const [loading, setLoading] = useState(false);


  // onSnapshot 
  
  // useEffect(() => {
  //   let isSubscribed = true;
  //   firestore().collection('sharedRecipes')
  //   .onSnapshot((querySnapshot) => {
  //     let listItems = [];
  //     querySnapshot.forEach((doc) => {
  //       if (isSubscribed) {
  //         // console.log(doc.id, '=>', doc.data());
  //         const { folder, recipe, isShared, likes } = doc.data();
  //         const userLikes = UserProxy.getLiked();
  //         if (userLikes) {
  //           isLiked = userLikes.indexOf(doc.id) >= 0 ? true : false;
  //         }   
  //         listItems.push({
  //           key: doc.id,
  //           recipe: recipe,
  //           folder: folder,
  //           likes: likes,
  //           isLiked: isLiked
  //         });
  //         setRecipes(listItems);
  //       }
  //     });
  //   })
  //   return () => isSubscribed = false;
  // },[]); 


  // get - still seems to refresh when data changes in the view

  // console.log(refreshData);

  useEffect(() => {
    //console.log('getting snapshot')
    let isSubscribed = true;
    setLoading(true);
    firestore().collection('sharedRecipes')
    .orderBy('recipe.date', 'desc')
    .startAfter(refreshData ? {} : lastDoc)
    .limit(5)      
    .get()
    .then((querySnapshot) => {
      let listItems = [];
      querySnapshot.forEach((doc) => {
        if (isSubscribed) {
          //console.log(doc.id, '=>', doc.data());
          const { folder, recipe, isShared, likes } = doc.data();
          const userLikes = UserProxy.getLiked();
          if (userLikes) {
            isLiked = userLikes.indexOf(doc.id) >= 0 ? true : false;
          }   
          listItems.push({
            key: doc.id,
            recipe: recipe,
            folder: folder,
            likes: likes,
            isLiked: isLiked
          });
        }
      });
      refreshData ? setRecipes(listItems)
        : setRecipes(previousData => [...previousData, ...listItems]);
      var lastVisible = querySnapshot.docs[querySnapshot.docs.length-1];
      if (lastVisible) {
        setTmpLastDoc(lastVisible);
      }
      setRefreshData(false);
      setLoading(false);
    });
    return () => isSubscribed = false;      
  },[refreshData, lastDoc]);  

  const updateRefresh = () => {
    setRefreshData(true);
  }

  const handleOnEndReached = useCallback(() => {
    setLastDoc(tmpLastDoc);
  });  

  if (recipes && isPad) {
    return (
      <View style={{backgroundColor: Colors.slate, flex:1}}>
        <FlatList
          data={recipes}
          numColumns={2}
          contentContainerStyle = {Styles.recipeList}
          columnWrapperStyle = {Styles.recipeListColumn}
          renderItem = {({item}) => <RecipeListItem item={item} navigation={navigation} updateRefresh={updateRefresh} />}
          keyExtractor={(item, index) => item.key}
          onRefresh={() => updateRefresh()}
          refreshing={loading}    
          onEndReachedThreshold={.5}
          onEndReached={() => handleOnEndReached()}         
        />
      </View>
    )
  } else if (recipes) {
    return (
      <View style={{backgroundColor: Colors.slate, flex:1}}>
        <FlatList
          data={recipes}
          contentContainerStyle = {Styles.recipeList}
          renderItem = {({item}) => <RecipeListItem item={item} navigation={navigation} updateRefresh={updateRefresh} />}
          keyExtractor={(item, index) => item.key}
          onRefresh={() => updateRefresh()}
          refreshing={loading}         
          onEndReachedThreshold={.5}
          onEndReached={() => handleOnEndReached()}              
        />
    </View>  
    )
  } 
  return (
    <View style={[Styles.containerGeneric, {justifyContent:'center'}]}>
      <Text style={Styles.defaultText}>No shared recipes yet</Text>
    </View>
  )

}

module.exports = Home;