import React, {useEffect, useState, useRef} from 'react';
import { Text, View, ScrollView, TouchableOpacity, Image, Animated} from 'react-native';
import PushNotificationIOS from "@react-native-community/push-notification-ios";
import KeepAwake from 'react-native-keep-awake';
import RBSheet from "react-native-raw-bottom-sheet"; 

import Styles from '../common/Styles';
import Colors from '../common/Colors';
import AsyncImage from '../components/AsyncImage';
import DataConstants from '../data/DataConstants';
import ShoppingListStorage from '../storage/ShoppingListStorage';
import Firebase from '../communication/Firebase';
import UserProxy from '../data/UserProxy';

let overlayNotificationAnim = new Animated.Value(-140);

const ViewRecipe = ({ route, navigation }) => {

  const { index, recipe, myRecipe, collectionRecipe } = route.params;
  const tagsAll = DataConstants.getTags();
  let [subscriber, setIsSubscriber] = useState(UserProxy.getIsSubscribed());
  let [overlayText, setOverlayText] = useState('')
  const refRBSheet = useRef();

  useEffect(() => {
    KeepAwake.activate();
    return () => KeepAwake.deactivate();
  },[]);
  
  useEffect(() => {
    const unsubscribe = navigation.addListener('focus', () => {
      setIsSubscriber(UserProxy.getIsSubscribed());
    });
    return () => unsubscribe();
  }, [navigation]);  

  getFormattedCookingTime = () => {
    const cookingHours = recipe.cookingHours == 0 ? '' : recipe.cookingHours + ' hrs ';
    const cookingMinutes = recipe.cookingMinutes + ' mins';
    return cookingHours + cookingMinutes;
  }

  getMSCookingTime = () => {
    const cookingTime = (recipe.cookingHours * 3600000) + (recipe.cookingMinutes * 60000);
    return cookingTime;
  }

  showHideOverlayMenuViewRecipe = () => {
    menuShown = !menuShown;
    Animated.timing(
      overlayMenuAnim,
      {
        toValue: !menuShown ? -500 : 0,
        duration: 200
      }
    ).start();     
  }; 

  const addToShoppingList = async () => {
    try {
      //console.log('adding..' + recipe.ingredients);
      await ShoppingListStorage.addToShoppingList(recipe.ingredients);
    } catch(e) {
      console.log(e);    
    }
    setOverlayText('Items added to the Shopping List')
    Animated.sequence([
      Animated.timing(
        overlayNotificationAnim, {
          toValue: 0,
          duration: 100
      }),
      Animated.timing(
        overlayNotificationAnim, {
          toValue: -140,
          delay: 2500,
          duration: 200
      })
    ]).start();    
  };

  const addToMyRecipes = () => {
    if (!subscriber) {
      navigation.navigate('Subscribe', {view:'Home', viewParams:{}});
    } else {      
      //console.log(recipe)
      Firebase.moveToMyRecipes(recipe, index)
        .then(() => {
          Firebase.updateFolders(0)
          setOverlayText('Added to My Recipes')
          Animated.sequence([
            Animated.timing(
              overlayNotificationAnim, {
                toValue: 0,
                duration: 100
            }),
            Animated.timing(
              overlayNotificationAnim, {
                toValue: -140,
                delay: 2500,
                duration: 200
            })
          ]).start();        
        })
        .catch((e) => console.log(e));
    }
  }

  const handleStartCooking = () => {
    if (!subscriber) {
      navigation.navigate('Subscribe', {view:'Home', viewParams:{}});
    } else {
      PushNotificationIOS.scheduleLocalNotification({
        alertBody: 'Your dish is ready!',
        //fireDate: new Date().toISOString() + (10 * 1000), //+10 seconds
        fireDate: new Date(Date.now() + getMSCookingTime()).toISOString()
      }); 
      navigation.navigate('Cooking', {cookingTime: getMSCookingTime()});      
    }
  }

  const renderIngredients = () => {
    return recipe.ingredients.map( function(ingredient, i) {
      return (
        <View style={[Styles.ingredientRow, {backgroundColor: i % 2 == 0 ? Colors.lightestGrey : Colors.altLightestGrey}]} key={i}>
          <Text style={[Styles.defaultText, {color:'#000', width:80, marginLeft:20}]}>{ingredient.quantity}</Text>
          <Text style={[Styles.defaultText, {color:'#000'}]}>{ingredient.ingredient}</Text>
        </View>
      )
    })
  };

  return (
    <View style={{flex:1}}>
      <ScrollView style={{flex:1, padding:0, backgroundColor: Colors.brightGreen}}>
        <AsyncImage index={index} isSplash hasImage={recipe.hasImage} />
        <View style={[Styles.paddedContainer, {backgroundColor: 'white'}]}>
          <Text style={Styles.defaultMainTitle}>{recipe.title}</Text>
        </View>
        <View style={[Styles.paddedContainer, {flexDirection:'row', backgroundColor: 'white', marginTop: -20}]}>

          {recipe.tags.length > 0 &&
            <View style={{flexDirection: 'row', alignItems: 'center'}}>
              {recipe.tags.map(function(tag, i) {
                return (
                  <Image source={tagsAll[tag].icon} style={{marginRight: 10}} key={i} />
                )})
              }
            </View>
          }

        </View>
        <View style={[Styles.paddedContainer, {paddingTop:0, backgroundColor: 'white'}]}>
          <View style={{flexDirection: 'row', height: 40, alignItems: 'center'}}>
            <Text style={[Styles.defaultTitle, {color:'#000', flex:1}]}>Ingredients</Text>
            <TouchableOpacity
              onPress={() => addToShoppingList()}>
              <Image source={require('../../img/icon-shoppinglist.png')} />
            </TouchableOpacity>
          </View>
        </View>
        {renderIngredients(recipe.ingredients)}
        <View style={[Styles.paddedContainer, {flex:1, backgroundColor: 'white'}]}>
          <View style={{flexDirection: 'row', height: 40, alignItems: 'center'}}>
            <Text style={[Styles.defaultTitle, {color:'#000', flex:1}]}>Method</Text>
          </View>
          <Text style={[Styles.defaultText, {color:'#000', lineHeight: 30, flex:1}]}>{recipe.method}</Text>
        </View>
        <View style={[Styles.paddedContainer, {backgroundColor:Colors.brightGreen, alignItems: 'center'}]}>
          { recipe.cookingHours > 0 || recipe.cookingMinutes > 0 &&
            <TouchableOpacity 
              onPress={() => handleStartCooking()}
              style={[Styles.button, {paddingLeft:0, marginBottom: 20, marginTop: 5}]}>
              <Text style={[Styles.buttonText, {flex:1, textAlign:'center'}]}>Start cooking ({getFormattedCookingTime()})</Text>
            </TouchableOpacity>
          }
          <Image source={require('../../img/leaves.png')} style={{marginBottom: 20}} />
        </View>
        <View style={Styles.viewRecipeHeader}>
          <TouchableOpacity onPress={() => navigation.goBack()}>
            <Image source={require('../../img/icon-back-reverse.png')} />
          </TouchableOpacity>
          { myRecipe || collectionRecipe &&
            <TouchableOpacity 
              style={{position: 'absolute', right: 0, top: 10}}
              hitSlop={{top:10, right:10, bottom:10 , left:10}}
              onPress={() => refRBSheet.current.open()}>
              <Image source={require('../../img/icon-edit-dots.png')} />
            </TouchableOpacity>          
          } 
        </View>        
      </ScrollView>

      <RBSheet
        ref={refRBSheet}
        closeOnDragDown={true}
        closeOnPressMask={true}
        height={120}
        customStyles={{
          wrapper: {
            backgroundColor: "rgba(0,0,0,0.2)"
          },
          draggableIcon: {
            backgroundColor: Colors.darkBlue
          }
        }}
      >

        { myRecipe &&
          <View style={{flex:1}}>
            <TouchableOpacity style={Styles.overlayMenuItem}
              hitSlop={{top:10, right: 10, bottom: 10, left: 10}}
              onPress={() => navigation.navigate('AddRecipe', { recipeData: recipe, recipeID: index, edit: true })}>
              <Text style={Styles.defaultTitle}>Edit</Text>
            </TouchableOpacity>
          </View>
        }

        { collectionRecipe &&
          <View style={{flex:1}}>
            <TouchableOpacity style={Styles.overlayMenuItem}
              hitSlop={{top:10, right: 10, bottom: 10, left: 10}}
              onPress={() => addToMyRecipes()}
            >
              <Text style={Styles.defaultTitle}>Add to My Recipes</Text>
            </TouchableOpacity>
          </View>
        }        
        
      </RBSheet>

      <Animated.View style={[Styles.overlayNotification, {top: overlayNotificationAnim}]}>
        <Image source={require('../../img/icon-tab-shoppinglist.png')} 
          style={{tintColor: Colors.darkBlue, marginBottom: 10, marginTop: 20}} />
        <Text style={Styles.defaultTitle}>{overlayText}</Text>
      </Animated.View>

      

    </View>
  );

};

export default ViewRecipe;
