import React, { useState, useEffect, useCallback } from 'react';
import { Text, View, FlatList, Image, TouchableOpacity, Dimensions } from 'react-native';
import firestore from '@react-native-firebase/firestore';

import Styles from '../common/Styles';
import Colors from '../common/Colors';
import UserProxy from '../data/UserProxy';
import RecipeListItem from '../components/RecipeListItem';

const CollectionRecipes = ({navigation, route}) => {

  const {width, height} = Dimensions.get('window');
  const isPad = height/width < 1.5 ? true : false;
  const {index, title} = route.params;
  const collectionID = index;
  const [lastDoc, setLastDoc] = useState({});
  const [tmpLastDoc, setTmpLastDoc] = useState({});    
  const [recipes, setRecipes] = useState([]);  
  const [refreshData, setRefreshData] = useState(false);
  const [loading, setLoading] = useState(false);

  navigation.setOptions({
    title:title,
    headerLeft: () => (
      <TouchableOpacity
        style={{marginLeft:20}}
        onPress={() => navigation.goBack()}
      >
        <Image source={require('../../img/icon-back-reverse.png')} />
      </TouchableOpacity>
    ),    
  }); 

  useEffect(() => {
    //console.log('getting data')
    let isSubscribed = true;
    setLoading(true);
    firestore().collection('recipeCollections').doc(collectionID).collection('recipes')
    .orderBy('recipe.date', 'desc')
    .startAfter(refreshData ? {} : lastDoc)
    .limit(5)  
    .get()
    .then((querySnapshot) => {
      let listItems = [];
      querySnapshot.forEach((doc) => {
        if (isSubscribed) {
          // console.log(doc.id, '=>', doc.data());
          const { folder, recipe, likes, isPremium } = doc.data();
          const userLikes = UserProxy.getLiked();
          if (userLikes) {
            isLiked = userLikes.indexOf(doc.id) >= 0 ? true : false;
          }   
          listItems.push({
            key: doc.id,
            recipe: recipe,
            folder: folder,
            likes: likes,
            isLiked: isLiked,
            isPremium: isPremium
          });
        }
      });
      refreshData ? setRecipes(listItems)
        : setRecipes(previousData => [...previousData, ...listItems])
      var lastVisible = querySnapshot.docs[querySnapshot.docs.length-1];
      if (lastVisible) {
        setTmpLastDoc(lastVisible);
      }        
      setRefreshData(false);
      setLoading(false);
    });
    return () => isSubscribed = false;        
  },[refreshData, lastDoc]);  

  const updateRefresh = () => {
    setRefreshData(true);
  }

  const handleOnEndReached = useCallback(() => {
    setLastDoc(tmpLastDoc);
  });  

  if (recipes && isPad) {
    return (
      <View style={{backgroundColor: Colors.slate, flex:1}}>
        <FlatList
          data={recipes}
          numColumns={2}
          contentContainerStyle = {Styles.recipeList}
          columnWrapperStyle = {Styles.recipeListColumn}
          onEndReachedThreshold={0.1}
         // onRefresh={() => updateRefresh()}
          //refreshing={loading}  
          onEndReached={() => handleOnEndReached()}               
          renderItem = {({item}) => 
            <RecipeListItem 
              item={item} 
              navigation={navigation} 
              updateRefresh={updateRefresh} 
              collectionRecipe={true} 
              collectionID={collectionID} />}
              keyExtractor={(item, index) => item.key}
            />
      </View>
    )
  } else if (recipes) {
    return (
      <View style={{backgroundColor: Colors.slate, flex:1}}>
        <FlatList
          data={recipes}
          contentContainerStyle = {Styles.recipeList}
          onEndReachedThreshold={0.1}
          //onRefresh={() => updateRefresh()}
         // refreshing={loading}  
          onEndReached={() => handleOnEndReached()}               
          renderItem = {({item}) => 
            <RecipeListItem 
              item={item} 
              navigation={navigation} 
              updateRefresh={updateRefresh} 
              collectionRecipe={true} 
              collectionID={collectionID} />}
              keyExtractor={(item, index) => item.key}
            />
      </View>
    )
  }
  return (
    <View style={[Styles.containerGeneric, {justifyContent:'center'}]}>
      <Text style={Styles.defaultText}>No shared recipes yet</Text>
    </View>
  )  

}

module.exports = CollectionRecipes;