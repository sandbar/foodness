import React, { useState, useEffect, useCallback, useRef } from 'react';
import { Text, View, FlatList, Image, TouchableOpacity, Dimensions } from 'react-native';
import firestore from '@react-native-firebase/firestore';
import RBSheet from "react-native-raw-bottom-sheet"; 

import Styles from '../common/Styles';
import Colors from '../common/Colors';
import UserProxy from '../data/UserProxy';
import DataConstants from '../data/DataConstants';
import RecipeListItem from '../components/RecipeListItem';

const MyRecipes = ({route, navigation}) => {

  const {folderIndex} = route.params;
  const {width, height} = Dimensions.get('window');
  const isPad = height/width < 1.5 ? true : false;
  const userID = UserProxy.getUID();
  const folderTitle = UserProxy.getFolders()[folderIndex].title;
  const [lastDoc, setLastDoc] = useState({});
  const [tmpLastDoc, setTmpLastDoc] = useState({});
  const [recipes, setRecipes] = useState([]);  
  const [refreshData, setRefreshData] = useState(true);
  const [loading, setLoading] = useState(false);
  let listItems = [];
  const refRBSheet = useRef();

  navigation.setOptions({
    headerRight: () => (
      <TouchableOpacity
        style={{marginRight:20, opacity:folderIndex == 0 ? 0 : 1}}
        hitSlop={{top:10, right:10, bottom:10, left:10}}
        onPress={() => refRBSheet.current.open()}
      >
        <Image source={require('../../img/icon-edit-dots.png')} />
      </TouchableOpacity>
    ),    
    headerLeft: () => (
      <TouchableOpacity
        style={{marginLeft:20}}
        hitSlop={{top:10, right:10, bottom:10, left:10}}
       //onPress={() => navigation.navigate('MyRecipes')}
        onPress={() => {
          menuShown = false;
          navigation.goBack();
        }}
      >
        <Image source={require('../../img/icon-back-reverse.png')} />
      </TouchableOpacity>
    ),
    headerStyle: {
      backgroundColor: DataConstants.getFolderColors()[folderIndex].color
    },
    title: folderTitle.toUpperCase()
  });    

  useEffect(() => {
    let isSubscribed = true;
    setLoading(true);
    firestore().collection('users').doc(userID).collection('myRecipes')
    .where('folder', '==', folderIndex)
    .orderBy('recipe.date', 'desc')
    .startAfter(lastDoc)
    .limit(5)
    .get()
    .then((querySnapshot) => {
      querySnapshot.forEach((doc) => {
        if (isSubscribed) {
          //console.log(doc.id, '=>', doc.data());
          const { folder, recipe, isShared, likes, isPremium } = doc.data();
          const userLikes = UserProxy.getLiked();
          if (userLikes) {
            isLiked = userLikes.indexOf(doc.id) >= 0 ? true : false;
          }      
          listItems.push({
            key: doc.id,
            recipe: recipe,
            folder: folder,
            likes: likes,
            isLiked: isLiked,
            isShared: isShared,
            isPremium: isPremium
          });    
        }
      });  
      refreshData ? setRecipes(listItems)
        : setRecipes(previousData => [...previousData, ...listItems])
      var lastVisible = querySnapshot.docs[querySnapshot.docs.length-1];
      if (lastVisible) {
        setTmpLastDoc(lastVisible);
      }
      setRefreshData(false);
      setLoading(false);        
    })
    return () => isSubscribed = false; 
  },[refreshData, lastDoc]);

  const handleOnEndReached = useCallback(() => {
    setLastDoc(tmpLastDoc);
  });

  const updateRefresh = () => {
    setLastDoc({});
    setRefreshData(true);
  }     

  deleteFolder = () => {
    // set all recipes to default folder
    const recipeCollectionRef = firestore().collection('users').doc(userID).collection('myRecipes');
    var userFolders = UserProxy.getFolders();
    var batch = firestore().batch();
    recipes.forEach((item) => {
      var itemRef = recipeCollectionRef.doc(item.key);
      batch.update(itemRef, {folder: 0});
      userFolders[0].total += 1;
    });
    batch.commit()
    .then(() => {
      //console.log('batch run successfully')
      const userRef = firestore().collection('users').doc(userID);
      userFolders.splice(folderIndex, 1);
      userRef.update({
        myFolders: userFolders
        //myFolders: firestore.FieldValue.arrayRemove(folderIndex)
      })
      .then(() => {
        //navigation.goBack();
        navigation.navigate('MyRecipes', {refreshMe:true})
      })
      .catch((e) => {console.log('error removing user folder:', e)});
    })
    .catch((e) => {console.log('batch error:', e)});
  }

  const getOverlayMenu = () => {
    return (
      <RBSheet
        ref={refRBSheet}
        closeOnDragDown={true}
        closeOnPressMask={true}
        height={180}
        customStyles={{
          wrapper: {
            backgroundColor: "rgba(0,0,0,0.2)"
          },
          draggableIcon: {
            backgroundColor: Colors.darkBlue
          }
        }}
      >
        <TouchableOpacity style={Styles.overlayMenuItem}
          onPress={() => {
            refRBSheet.current.close();
            deleteFolder()
          }}
        >
          <Text style={Styles.defaultTitle}>Delete Folder</Text>
        </TouchableOpacity>
        <View style={Styles.overlayMenuSeperator} />
        <TouchableOpacity style={Styles.overlayMenuItem}
          onPress={() => {
            navigation.navigate('AddFolder', {title: 'Rename Folder', rename: true, folderIndex: folderIndex});
            refRBSheet.current.close(); 
        }}
        >
          <Text style={Styles.defaultTitle}>Rename</Text>
        </TouchableOpacity>
      </RBSheet>   
    )
  }

  if (recipes.length > 0 && isPad) {
    return (
      <View style={{backgroundColor: Colors.slate, flex:1}}>
        <FlatList
          data={recipes}
          numColumns={2}
          contentContainerStyle = {Styles.recipeList}
          columnWrapperStyle = {Styles.recipeListColumn}
          renderItem = {({item}) => <RecipeListItem item={item} navigation={navigation} myRecipe />}
          keyExtractor={(item, index) => item.key}
          onRefresh={() => updateRefresh()}
          refreshing={loading}            
          onEndReachedThreshold={1}
          onEndReached={() => handleOnEndReached()}
        />      
        {getOverlayMenu()}         
      </View>
    )
  } else if (recipes.length > 0) {
    return (
      <View style={{backgroundColor: Colors.slate, flex:1}}>
        <FlatList
          data={recipes}
          contentContainerStyle = {Styles.recipeList}
          renderItem = {({item}) => <RecipeListItem item={item} navigation={navigation} myRecipe />}
          keyExtractor={(item, index) => item.key}
          onRefresh={() => updateRefresh()}
          refreshing={loading}            
          onEndReachedThreshold={1}
          onEndReached={() => handleOnEndReached()}
        />      
        {getOverlayMenu()}         
      </View>
    )
  };
  return (
    <View style={[Styles.containerGeneric, {justifyContent:'center'}]}>
      <Image source={require('../../img/icon-drawer-recipes.png')} style={{marginBottom: 20}} />
      <Text style={Styles.defaultText}>You have no recipes yet</Text>
      {getOverlayMenu()}
    </View>
  )  
}

export default MyRecipes;