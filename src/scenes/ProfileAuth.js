import React, { useState } from 'react';
import { Text, View, TextInput, TouchableOpacity, Image, SafeAreaView } from 'react-native';
import auth from '@react-native-firebase/auth';
import firestore from '@react-native-firebase/firestore';

import Styles from '../common/Styles';
import UserProxy from '../data/UserProxy';

const ProfileAuth = ({navigation}) => {

  const [email, setEmail] = useState('');
  const [password, setPassword] = useState('');
  const [emailError, setEmailError] = useState('');
  const [emailValid, setEmailValid] = useState(false);
  const [passwordVisible, setPasswordVisible] = useState(false);
  const [passwordError, setPasswordError] = useState('');
  const [loading, setLoading] = useState(false);

  navigation.setOptions({  
    headerLeft: () => (
      <TouchableOpacity
        style={{marginLeft:20}}
        onPress={() => navigation.goBack()}
      >
        <Image source={require('../../img/icon-back-reverse.png')} />
      </TouchableOpacity>
    )
  });   

  const handleContinuePress = () => {
    if (emailValid && password.length === 0) {
      setPasswordVisible(true); 
      setPasswordError('');
    } else if (emailValid && password.length > 6 ) {
      linkUserAccounts();
    } else {
      setPasswordError('Enter a password longer than 6 characters');
    }
  }  

  const handleEmailInput = (input) => {
    if (/^\w+([\.-]?\w+)*@\w+([\.-]?\w+)*(\.\w{2,3})+$/.test(input)) {
      setEmailValid(true);
    }
  }

  const linkUserAccounts = async () => {
    setLoading(true);
    let credential = auth.EmailAuthProvider.credential(email, password);
    auth().currentUser.linkWithCredential(credential)
    .then((userCredential) => {
      console.log('accounts linked', userCredential.user.toJSON())
      const newUserJson = userCredential.user.toJSON();
      const userRef = firestore().collection('users').doc(newUserJson.uid); 
      userRef.get()
      .then((docSnapshot) => {
        if (docSnapshot.exists) {
          // user exists, which we expect. Merge data
          userRef.get().then((doc) => {
            const user = doc.data();
            //console.log('old user', user)
            //console.log('new user json', newUserJson)
            userRef.update({userJson: newUserJson})
            UserProxy.storeUserData(newUserJson);
            navigation.navigate('Profile', {refreshMe: true});
          })
          .catch(e => console.log(e));
        } 
      });       
    })
    .catch((e) => {
      console.log('linking error', e);
      if (e.code == 'auth/email-already-in-use') { 
        setLoading(false);
        setEmailError('This email is already being used');        
      }
    })
  };

  if (loading) {
    return (
      <View style={Styles.loadingContainer}>
        <Image source={require('../../img/leaves.png')} />
      </View>        
    )
  } else {
    return (
      <View style={Styles.authContainer}>
        <SafeAreaView>
          <View style={{padding:20}}>
            <TextInput
              style={[Styles.textInput,
                { marginBottom: 20,
                  paddingBottom: 0,
                  fontSize: 22,
                  color: '#ffffff'
                }]}
              value={email}
              onChangeText={(inputText) => {
                setEmail(inputText)
                setEmailError('');
                handleEmailInput(inputText);
              }}
              //placeholderTextColor={'rgba(255,255,255,0.5)'}
              onSubmitEditing={() => handleContinuePress()}
              autoCompleteType={'email'}
              keyboardType={'email-address'}
              textContentType={'emailAddress'}
              returnKeyType={'next'}
              autoCapitalize={'none'}
              autoCorrect={false}
              underlineColorAndroid='transparent'
              placeholder={'Email'} />  
            {emailError !== '' &&
              <View style={{flexDirection: 'row', alignItems: 'center', justifyContent: 'flex-end', marginBottom:20, marginTop: -10}}>
                <Text style={[Styles.smallText, {color: '#ff0000'}]}>{emailError}</Text>
                <View style={Styles.errorIcon}>
                  <Text style={[Styles.smallText, {color: '#fff'}]}>!</Text>
                </View>
              </View>      
            }  
            {passwordVisible &&
              <TextInput
                style={[Styles.textInput,
                  { marginBottom: 20,
                    paddingBottom: 0,
                    fontSize: 22,
                    color: '#ffffff'
                  }]}
                value={password}
                onChangeText={(inputText) => {
                  setPassword(inputText)
                  setPasswordError('');
                }}
                onSubmitEditing={() => handleContinuePress()}
                autoCompleteType={'password'}
                secureTextEntry
                textContentType={'password'}
                returnKeyType={'done'}
                underlineColorAndroid='transparent'
                placeholder={'Password'} />            
            }  
            {passwordError !== '' &&
              <View style={{flexDirection: 'row', alignItems: 'center', justifyContent: 'flex-end', marginBottom:20, marginTop: -10}}>
                <Text style={[Styles.smallText, {color: '#ff0000'}]}>{passwordError}</Text>
                <View style={Styles.errorIcon}>
                  <Text style={[Styles.smallText, {color: '#fff'}]}>!</Text>
                </View>
              </View>      
            }            
            {emailValid &&
              <TouchableOpacity
                style={[Styles.button, {alignSelf:'stretch', justifyContent: 'center', height: 50}]}
                onPress={() => handleContinuePress()}>
                <Text style={Styles.buttonText}>Continue</Text>
              </TouchableOpacity>   
            }           
          </View>
        </SafeAreaView>
        <Image source={require('../../img/splash-footer.png')} style={Styles.splashFooter} />
      </View>
    )
  }

};

module.exports = ProfileAuth;