import React, {useEffect, useState} from 'react';
import { ActivityIndicator, Text, View, ScrollView, TouchableOpacity, Image, Platform, Alert, Linking} from 'react-native';
import RNIap, {
  purchaseErrorListener,
  purchaseUpdatedListener,
  ProductPurchase,
  PurchaseError
} from 'react-native-iap';

import Colors from '../common/Colors';
import Styles from '../common/Styles';
import Purchase from '../communication/Purchase';
import DataProxy from '../data/DataProxy';
import UserProxy from '../data/UserProxy';
import Firebase from '../communication/Firebase';

const itemSubs = Platform.select({
  ios: [
    'subscription.monthly'
  ],
  android: [
    'subscription.monthly'
  ]
});

const Subscribe = ({ navigation, route }) => {

  const {view, viewParams} = route.params;
  const [loading, setLoading] = useState(false);
  const [errorShown, setErrorShown] = useState(false)
  const [localizedPrice, setLocalizedPrice] = useState('');

  const termsURL = 'https://foodnessapp.co/terms-en/';
  const privacyURL = 'https://foodnessapp.co/privacy-en/';

  useEffect(() => {

    Purchase.getSubscriptions(itemSubs)
      .then((subscriptions) => {
        // console.log("subscriptions:", subscriptions)
        DataProxy.setSubscriptions(subscriptions);
        setLocalizedPrice(subscriptions[0].localizedPrice);
      })
      .catch((e) => {console.log(e)});

      const purchaseUpdateSubscription = purchaseUpdatedListener(( purchase ) => {
        // console.log('purchaseUpdatedListener', purchase);
        const receipt = purchase.transactionReceipt;
        if ( receipt ) {
          Firebase.updateSubscription( purchase )
          .then(( response ) => {
            if ( response ) {
              RNIap.finishTransaction(purchase, false)
              .then(() => {
                console.log('setting userproxy status')
                UserProxy.setIsSubscribed(true);
                setLoading(false);
                setTimeout(navigation.goBack, 1000);
                //navigation.navigate(view, viewParams);
              })
            } else {
              console.log('there was an error finishing the transaction');
            }
          })
        }
      })
  
      const purchaseErrorSubscription = purchaseErrorListener((e) => {
        console.log('purchaseErrorListener', e);
        handleSubscribeError(e);
      });    
  
  
      return function cleanup() {
        purchaseUpdateSubscription.remove();
        purchaseErrorSubscription.remove();
      }      

  },[]);   

  return (
    <View style={{ flex:1, backgroundColor: Colors.darkBlue }}>
      <Image source={require('../../img/splash-iap.jpg')} style={Styles.subscribeImage} />
      {/* <ScrollView style={Styles.subscribeScrollView}> */}
        <View style={Styles.subscribeCard}>
          <Image source={require('../../img/leaves.png')} style={{alignSelf: 'center', marginBottom: 20}} />
          <Text style={[Styles.defaultMainTitle, {color: Colors.darkBlue, textAlign: 'center', marginBottom: 30}]}>Get Foodness Premium</Text>
          <ScrollView style={{flex:1}}>
            <View style={Styles.subscribeRow}>
              <View style={Styles.subscribeTick}>
                <Image source={require('../../img/icon-tick.png')} />
              </View>
              <Text style={[Styles.defaultTitle, {flex:1}]}>Premium curated recipes</Text>                      
            </View>
            <View style={Styles.subscribeRow}>
              <View style={Styles.subscribeTick}>
                <Image source={require('../../img/icon-tick.png')} />
              </View>
              <Text style={Styles.defaultTitle}>Store unlimited recipes</Text>                      
            </View>    
            <View style={Styles.subscribeRow}>
              <View style={Styles.subscribeTick}>
                <Image source={require('../../img/icon-tick.png')} />
              </View>
              <Text style={Styles.defaultTitle}>Upgraded recipe tools</Text>                      
            </View>  
            <TouchableOpacity
              style={[Styles.button, {alignSelf:'stretch', justifyContent: 'center', marginTop: 10}]}
              onPress={() => handleSubscribePress()}>
                {loading &&
                  <ActivityIndicator size='small' color='#ffffff' />
                }
                {!loading &&
                  <Text style={Styles.buttonText}>Try it free for 30 days</Text>
                }
            </TouchableOpacity> 
            <Text style={[Styles.defaultText, {flex:1, textAlign: 'center', marginTop: 15, fontSize: 12}]}>Then {localizedPrice} per month</Text>
            <TouchableOpacity 
              onPress={() => handleRestorePurchases()}>
              <Text style={[Styles.defaultText, {flex:1, textAlign: 'center', marginTop: 20}]}>Restore purchases</Text>
            </TouchableOpacity>        
            <Text style={[Styles.defaultText, {marginTop: 15, fontSize: 12}]}>
              Subscriptions will be charged to your credit card through your iTunes account. Your subscription will automatically renew unless cancelled at least 24 hours before the end of the current period. You will not be able to cancel the subscription once activated. Manage your subscriptions in Account Settings after purchase.
            </Text>
            <TouchableOpacity
              onPress={() => handleShowURL(termsURL)}>
                <Text style={[Styles.defaultText, {marginTop: 15, fontSize: 12, flex: 1, textAlign: 'center'}]}>
                  Terms of Service
                </Text>
            </TouchableOpacity>
            <TouchableOpacity
              onPress={() => handleShowURL(privacyURL)}>
                <Text style={[Styles.defaultText, {marginTop: 15, fontSize: 12, flex: 1, textAlign: 'center'}]}>
                  Privacy Policy
                </Text>
            </TouchableOpacity>            
            </ScrollView>                   
        </View>
      {/* </ScrollView>      */}
      <TouchableOpacity
        onPress={() => navigation.goBack()}
        //disabled={loading}
        hitSlop={{top: 10, right: 10, bottom: 10, left: 10}}
        style={{position: 'absolute', right: 30, top: 30}}
      >
        <Image source={require('../../img/icon-close.png')} style={{tintColor: '#FFFFFF'}} />
      </TouchableOpacity>
    </View>
  )

  async function handleRestorePurchases() {
    setLoading(true);
    try {
      const purchases = await RNIap.getAvailablePurchases();
      let isSubscriber = false;
      let restoredTitles = [];

      //console.log(purchases)

      purchases.forEach(purchase => {
        switch (purchase.productId) {
          case 'subscription.monthly' :
            const receiptBody = {
              'receipt-data': purchase.transactionReceipt,
              'password': '616eeac553ce4fc3a437ae0ed96b68a1'
            };
            Purchase.validateReceipt(receiptBody)
            .then((response) => {
              //console.log('validateReceipt', response);
              const lastTransaction = response.latest_receipt_info.length - 1;
              const receiptDate = parseInt(response.latest_receipt_info[lastTransaction].expires_date_ms);
              if (receiptDate > Date.now()) {
                console.log('subscriber!')
                UserProxy.setIsSubscribed(true);
                isSubscriber = true;
                //Alert.alert('Restore Successful', 'You sucessfully restored the following purchases: Monthly Subscription');
                navigation.goBack();
              } else {
                console.log('expired subscriber')
                UserProxy.setIsSubscribed(false);
                setLoading(false);
              }
            })
            .catch((e) => {
              console.log('e');
            })                        
            //restoredTitles.push('Monthly Subscription');
            break;
        }
      });
      if (!isSubscriber) {
        Alert.alert('Restore Successful', 'Your Monthly Subscription has expired');
      }
    } catch(e) {
      console.log(e);
      Alert.alert(e.message);
    }
  }

  async function handleSubscribePress() {
    setLoading(true);

    // LIVE
    if (Platform.OS === 'ios') {
      await RNIap.clearTransactionIOS();
    }

    const productID = itemSubs[0];
    try {
      await RNIap.requestSubscription(productID, false).catch(e => handleSubscribeError(e));
    } catch (e) {
      handleSubscribeError(e);
    }

    // DEBUG
    // UserProxy.setIsSubscribed(true);
    // navigation.navigate(view, viewParams);
  }

  function handleSubscribeError(e) {
    console.log(e)
    setLoading(false);
      if (!errorShown) {
        switch(e.code) {
          case ('E_UNKNOWN'): {
            Alert.alert(
              'Connection problem',
              'There was a problem connecting to the App Store. Please try again.',
              [{text:'0K'}],
              {cancelable: true}
            )
            setErrorShown(true);
            break;
          }
          case ('E_USER_CANCELLED'): {
            Alert.alert(
              'Connection problem',
              'There was a problem connecting to the App Store. Please try again.',
              [{text:'0K'}],
              {cancelable: true}
            )
            setErrorShown(true);
            break;
          }
          default: {
            Alert.alert(
              'Connection problem',
              'There was a problem connecting to the App Store. Please try again.',
              [{text:'0K'}],
              {cancelable: true}
            )   
            setErrorShown(true);     
          }
        }
    }
  }  

  async function handleShowURL(url) {
    const supported = await Linking.canOpenURL(url);
    if (supported) {
      await Linking.openURL(url);
    }
  }

}

export default Subscribe;