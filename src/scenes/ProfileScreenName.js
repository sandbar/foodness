import React, {useState} from 'react';
import { Text, View, TextInput, TouchableOpacity, Dimensions, Image } from 'react-native';

import Firebase from '../communication/Firebase';
import Styles from '../common/Styles';
import UserProxy from '../data/UserProxy';

const ProfileAnonymous = ({navigation}) => {

  const {width, height} = Dimensions.get('window');
  const [screenName, setScreenName] = useState(UserProxy.getScreenName() ? UserProxy.getScreenName() : '');
  const [screenNameError, setScreenNameError] = useState('');
  const [edited, setEdited] = useState(false);

  navigation.setOptions({  
    headerLeft: () => (
      <TouchableOpacity
        style={{marginLeft:20}}
        onPress={() => navigation.goBack()}
        hitSlop={{top:10, right:10, bottom:10, left:10}}
      >
        <Image source={require('../../img/icon-back-reverse.png')} />
      </TouchableOpacity>
    )
  }); 

  const handleSavePress = () => {

    const screenNameValid = validateScreenName();

    if (!screenNameValid) {
      setScreenNameError('Screen name should not use special characters')
    } else {
      Firebase.addUsername(screenName, edited)
      .then((response) => {
        if (response == undefined) {
          setScreenNameError('Screen name already exists');
        } else {
          console.log('username added');
          Firebase.addScreenName(screenName)
          .then((response) => {
            navigation.navigate('Profile', {refreshMe: true});
          })
          .catch((e) => {
            console.log('problem adding username', e);
          })        
        }
      })
      .catch((e) => {
        console.log('problem adding username', e);
      }) 
    }
  };

  validateScreenName = () => {
    //return screenName.match('^([a-z0-9_.]){5,30}$') ? true : false;
    return screenName.match('^([A-Za-z0-9_.]){5,30}$') ? true : false;
  }

  return (
    <View style={Styles.authContainer}>
      <View style={{padding: 20}}> 
        <View style={{ width: width - 40}}>
          <TextInput
            style={[Styles.textInput,
              { marginBottom: 20,
                paddingBottom: 0,
                fontSize: 22,
                color: '#ffffff'
              }]}
            value={screenName}
            onChangeText={(inputText) => {
              setScreenName(inputText)
              setScreenNameError('');
              setEdited(true);
            }}
            onSubmitEditing={() => handleSavePress()}
            keyboardType={'default'}
            returnKeyType={'next'}
            autoCorrect={false}
            autoCapitalize={'none'}
            underlineColorAndroid='transparent'
            placeholder={'Choose a screen name'} />  
          {screenNameError !== '' &&
            <View style={{flexDirection: 'row', alignItems: 'center', justifyContent: 'flex-end', marginBottom:20, marginTop: -10}}>
              <Text style={[Styles.smallText, {color: '#ff0000'}]}>{screenNameError}</Text>
              <View style={Styles.errorIcon}>
                <Text style={[Styles.smallText, {color: '#fff'}]}>!</Text>
              </View>
            </View>      
          }    
          <TouchableOpacity
            style={[Styles.button, {alignSelf:'stretch', justifyContent: 'center', opacity: !edited ? 0.3 : 1, height: 50}]}
            disabled={!edited ? true : false}
            onPress={() => handleSavePress()}>
            <Text style={Styles.buttonText}>Save</Text>
          </TouchableOpacity>           
        </View>         
      </View>
    </View>
  )

}

module.exports = ProfileAnonymous;