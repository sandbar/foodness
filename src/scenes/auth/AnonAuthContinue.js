import React, { useContext, useState } from 'react';
import { Text, View, TouchableOpacity, Image } from 'react-native';
import auth from '@react-native-firebase/auth';
import firestore from '@react-native-firebase/firestore';
import {AuthContext} from '../../../App';

import Styles from '../../common/Styles';
import UserProxy from '../../data/UserProxy';

const AnonAuthContinue = ({navigation}) => {

  const { signIn } = useContext(AuthContext);
  const [loading, setLoading] = useState(false);

  const handleCreateAnonAccount = async () => {
    setLoading(true);
    let credential;
    try {
      credential = await auth().signInAnonymously();
    } catch (e) {
      console.log(e);
    }
    const userJson = credential.user.toJSON();
    UserProxy.storeUserData(userJson);
    const userRef = firestore().collection('users').doc(userJson.uid);    
    userRef.get()
    .then((docSnapshot) => {
      if (docSnapshot.exists) {
        // user exists - which would be odd
      } else {
        // add the user to FS
        userRef.set({userJson})
        .then (() => {
          console.log('user added successfully');
          userRef.update({
            myFolders: firestore.FieldValue.arrayUnion({title: 'default', total: 0})
          })
          .then (() => {
            console.log('default folder added successfully')
            UserProxy.storeFolders([{title: 'default', total: 0}]);
            UserProxy.setIsSubscribed(false);
            signIn();
          })
          .catch((error) => {
            console.log('error adding folder', error)
          })
        });
      }
    });    
  }

  if (loading) {
    return (
      <View style={Styles.loadingContainer}>
        <Image source={require('../../../img/leaves.png')} />
      </View>        
    )
  } else {
    return (
      <View style={[Styles.authContainer, {justifyContent: 'center'}]}>
        <View style={{padding:20,  alignItems: 'center', marginTop: -100}}>
          <Text style={[Styles.defaultText, {textAlign: 'center', marginBottom: 30}]}>
            Are you sure you don't want to create an account and save your recipes safely?
          </Text>
          <TouchableOpacity
            style={[Styles.button, {alignSelf:'stretch', justifyContent: 'center', height: 50}]}
            onPress={() => handleCreateAnonAccount()}>
            <Text style={Styles.buttonText}>
              Yes, I’m good with that
            </Text>
          </TouchableOpacity>    
          <TouchableOpacity
            onPress={() => navigation.goBack()}
            style={{marginTop:30}}>
            <Text style={[Styles.defaultText, {fontSize:15, fontFamily: 'HelveticaNeue-Bold', color: '#fff'}]}>
              OK, create an account!
            </Text>
          </TouchableOpacity>     
        </View> 
        <Image source={require('../../../img/splash-footer.png')} style={Styles.splashFooter} />
      </View>
    )
  }

};

module.exports = AnonAuthContinue;