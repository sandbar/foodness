import React, {useState, useContext} from 'react';
import {
  Text,
  View,
  TextInput,
  TouchableOpacity,
  Image,
  SafeAreaView,
} from 'react-native';
import auth from '@react-native-firebase/auth';
import firestore from '@react-native-firebase/firestore';
import {AuthContext} from '../../../App';

import Styles from '../../common/Styles';
import UserProxy from '../../data/UserProxy';

const Auth = ({navigation}) => {
  const [email, setEmail] = useState('');
  const [password, setPassword] = useState('');
  const [emailError, setEmailError] = useState('');
  const [emailValid, setEmailValid] = useState(false);
  const [passwordVisible, setPasswordVisible] = useState(false);
  const [passwordError, setPasswordError] = useState('');
  const [loading, setLoading] = useState(false);
  const [forgotPassword, setForgotPassword] = useState(false);
  const {signIn} = useContext(AuthContext);

  const handleContinuePress = () => {
    if (!forgotPassword) {
      if (emailValid && password.length === 0) {
        setPasswordVisible(true);
        setPasswordError('');
      } else if (emailValid && password.length > 6 ) {
        authUser();
      } else {
        setPasswordError('Enter a password longer than 6 characters');
      }
    } else {
      sendForgotEmail();
    }
  };

  const handleEmailInput = input => {
    if (/^\w+([\.-]?\w+)*@\w+([\.-]?\w+)*(\.\w{2,3})+$/.test(input)) {
      setEmailValid(true);
    }
  };

  const authUser = async () => {
    setLoading(true);
    let credential;
    try {
      credential = await auth().signInWithEmailAndPassword(email, password);
    } catch (error) {
      console.log(error.code);
      console.log(error.message);
      if (error.code === 'auth/user-not-found') {
        try {
          credential = await auth().createUserWithEmailAndPassword(
            email,
            password,
          );
        } catch (error) {
          if (error.code === 'auth/email-already-in-use') {
            // handle this case? - not sure it's needed
            setLoading(false);
            setEmailError('This email is already being used');
          }
        }
      } else if (e.code === 'auth/wrong-password') {
        setLoading(false);
        setPasswordError('Password and email do not match');
        setForgotPassword(true);
      }
    }
    if (credential) {
      const userJson = credential.user.toJSON();
      UserProxy.storeUserData(userJson);
      const userRef = firestore()
        .collection('users')
        .doc(userJson.uid);
      userRef.get().then(docSnapshot => {
        if (docSnapshot.exists) {
          // user exists - they are signing back in
          userRef.get().then(doc => {
            const user = doc.data();
            console.log(user)
            UserProxy.storeUserData(user.userJson);
            // store liked
            if (user.myLikedRecipes) {
              UserProxy.storeLiked(user.myLikedRecipes);
            }
            if (user.myFolders) {
              UserProxy.storeFolders(user.myFolders);
            }
            if (user.subscriptionReceipt) {
              const receiptBody = {
                'receipt-data': user.subscriptionReceipt.transactionReceipt,
                'password': '616eeac553ce4fc3a437ae0ed96b68a1',
              };
              Purchase.validateReceipt(receiptBody)
                .then(response => {
                  // console.log('validateReceipt', response);
                  const lastTransaction = response.latest_receipt_info.length - 1;
                  const receiptDate = parseInt(response.latest_receipt_info[lastTransaction].expires_date_ms);
                  if (receiptDate > Date.now()) {
                    console.log('subscriber!');
                    UserProxy.setIsSubscribed(true);
                  } else {
                    console.log('expired subscriber')
                    UserProxy.setIsSubscribed(false);
                  }
              })
              .catch((e) => {
                console.log('e');
              })
            } else {
              UserProxy.setIsSubscribed(false);
            }
          });
          dispatch({ type: 'IS_AUTHENTICATED' });          
        } else {
          // add the user to FS
          userRef.set({userJson})
          .then (() => {
            console.log('user added successfully');
            userRef.update({
              myFolders: firestore.FieldValue.arrayUnion({title: 'default', total: 0})
            })
            .then (() => {
              console.log('default folder added successfully')
              UserProxy.storeFolders({title: 'default', total: 0});
              UserProxy.setIsSubscribed(false);
              signIn();
            })
            .catch((error) => {
              console.log('error adding folder', error)
            })
          });
        }
      });      
    }
  };

  const sendForgotEmail = async () => {
    var actionCodeSettings = {
      url: 'https://foodnessapp.co/?email=user@example.com',
      iOS: {
        bundleId: 'co.foodness.app',
      },
      handleCodeInApp: false,
    };
    try {
      await auth().sendPasswordResetEmail(email, actionCodeSettings)
    } catch (e) {
      console.log(e);
      if (e.code === 'auth/user-not-found') {
        setEmailError('Email address was not found');
      }
    }
  };

  const handleForgotPassword = () => {
    setForgotPassword(true);
  };

  if (loading) {
    return (
      <View style={Styles.loadingContainer}>
        <Image source={require('../../../img/leaves.png')} />
      </View>
    )
  } else {
    return (
      <View style={Styles.authContainer}>
        <SafeAreaView>
          <View style={{padding:20}}>
            {emailValid && !forgotPassword ? (
              <View style={{flexDirection: 'row', height: 45}}>
                <TouchableOpacity onPress={() => {
                  setEmail(''); 
                  setEmailValid(false); 
                  setPasswordVisible(false);
                  setForgotPassword(false);
                }}>
                  <Image source={require('../../../img/icon-back-reverse.png')} />
                </TouchableOpacity>
                <Text style={[Styles.defaultTitle, {color:'#ffffff', marginBottom: 20, textAlign:'center', flex:1}]}>Create account / sign in</Text>
              </View>
            )
            :
            (
              <View style={{flexDirection: 'row', height: 45}}>
                <Text style={[Styles.defaultTitle, {color:'#ffffff', marginBottom: 20, textAlign:'center', flex:1}]}>Get started with an email address</Text>
              </View>
            )
            }
            {forgotPassword &&
              <View style={{flexDirection: 'row', height: 45}}>
              <TouchableOpacity onPress={() => {
                setEmail(''); 
                setEmailValid(false); 
                setPasswordVisible(false);
                setForgotPassword(false);
              }}>
                <Image source={require('../../../img/icon-back-reverse.png')} />
              </TouchableOpacity>                
              <Text style={[Styles.defaultTitle, {color:'#ffffff', marginBottom: 20, textAlign:'center', flex:1}]}>Forgot password</Text>
            </View>            
            }
            <TextInput
              style={[Styles.textInput,
                { marginBottom: 20,
                  paddingBottom: 0,
                  fontSize: 22,
                  color: '#ffffff'
                }]}
              value={email}
              onChangeText={(inputText) => {
                setEmail(inputText)
                setEmailError('');
                handleEmailInput(inputText);
              }}
              //placeholderTextColor={'rgba(255,255,255,0.5)'}
              onSubmitEditing={() => handleContinuePress()}
              autoCompleteType={'email'}
              keyboardType={'email-address'}
              textContentType={'emailAddress'}
              returnKeyType={'next'}
              autoCapitalize={'none'}
              autoCorrect={false}
              underlineColorAndroid='transparent'
              placeholder={'Email'} />  
            {emailError !== '' &&
              <View style={{flexDirection: 'row', alignItems: 'center', justifyContent: 'flex-end', marginBottom:20, marginTop: -10}}>
                <Text style={[Styles.smallText, {color: '#ff0000'}]}>{emailError}</Text>
                <View style={Styles.errorIcon}>
                  <Text style={[Styles.smallText, {color: '#fff'}]}>!</Text>
                </View>
              </View>      
            }  
            {passwordVisible &&
              <TextInput
                style={[Styles.textInput,
                  { marginBottom: 20,
                    paddingBottom: 0,
                    fontSize: 22,
                    color: '#ffffff'
                  }]}
                value={password}
                onChangeText={(inputText) => {
                  setPassword(inputText)
                  setPasswordError('');
                }}
                onSubmitEditing={() => handleContinuePress()}
                autoCompleteType={'password'}
                secureTextEntry
                textContentType={'password'}
                returnKeyType={'done'}
                underlineColorAndroid='transparent'
                placeholder={'Password'} />            
            }  
            {passwordError !== '' &&
              <View style={{flexDirection: 'row', alignItems: 'center', justifyContent: 'flex-end', marginBottom:20, marginTop: -10}}>
                <Text style={[Styles.smallText, {color: '#ff0000'}]}>{passwordError}</Text>
                <View style={Styles.errorIcon}>
                  <Text style={[Styles.smallText, {color: '#fff'}]}>!</Text>
                </View>
              </View>      
            }            
            {emailValid ? (
              <TouchableOpacity
                style={[Styles.button, {alignSelf:'stretch', justifyContent: 'center', height: 50}]}
                onPress={() => handleContinuePress()}>
                <Text style={Styles.buttonText}>Continue</Text>
              </TouchableOpacity>   
              )
              :
              (
              <TouchableOpacity
                onPress={() => navigation.navigate('AnonAuthContinue')}
                style={{alignSelf: 'center', marginTop: 20}}>
                <Text
                  style={[
                    Styles.defaultText,
                    {
                      fontSize: 15,
                      fontFamily: 'HelveticaNeue-Bold',
                      color: '#fff',
                    },
                  ]}>
                  Continue as guest
                </Text>
              </TouchableOpacity>
              )
            }
            {forgotPassword && (
              <TouchableOpacity
                onPress={() => handleForgotPassword()}
                style={{alignSelf: 'center', marginTop: 20}}>
                <Text
                  style={[
                    Styles.defaultText,
                    {
                      fontSize: 15,
                      fontFamily: 'HelveticaNeue-Bold',
                      color: '#fff',
                    },
                  ]}>
                  Forgot password
                </Text>
              </TouchableOpacity>
            )}
          </View>
        </SafeAreaView>
        <Image
          source={require('../../../img/splash-footer.png')}
          style={Styles.splashFooter}
        />
      </View>
    );
  }
};

module.exports = Auth;
