import React from 'react';
import { Text, View, FlatList } from 'react-native';
//import firebase from 'react-native-firebase';
import firestore from '@react-native-firebase/firestore';

import Styles from '../common/Styles';
import Colors from '../common/Colors';
import UserProxy from '../data/UserProxy';
import RecipeListItem from '../components/RecipeListItem';

export default class Liked extends React.Component {
  
  constructor(props, context) {
    super(props, context);
    this.ref = firestore().collection('users').doc(UserProxy.getUID()).collection('myLikedRecipes');
    this.unsubscribe = null;       
    this.state = {
    }
  }

  componentDidMount() {
    this.getSnapshot();
  }

  componentWillUnmount() {
    if (this.unsubscribe) {
      this.unsubscribe();
    }
  }

  getSnapshot() {
    this.unsubscribe = this.ref.onSnapshot(this.onCollectionUpdate);
  }

  onCollectionUpdate = (querySnapshot) => {
    this.listItems = [];
    querySnapshot.forEach((doc) => {
      //console.log(doc.id, '=>', doc.data());
      const { recipe } = doc.data();
      const liked = UserProxy.getLiked();
      if (liked) {
        recipe.isLiked = liked.indexOf(doc.id) >= 0 ? true : false;
      }      
      this.listItems.push({
        key: doc.id,
        recipe: recipe
      })
    });
    //console.log(this.listItems);
    if (this.listItems.length > 0) {
      this.setState({
        recipes: this.listItems,
      });
    }

  }  

  render() {
    if (this.state.recipes) {
      return (
        <View style={{backgroundColor: Colors.slate, flex:1}}>
          <FlatList
            data={this.state.recipes}
            contentContainerStyle = {Styles.recipeList}
            renderItem = {({item}) => <RecipeListItem item={item} navigation={this.props.navigation} />}
            keyExtractor={(item, index) => item.key}
          />
        </View>
      )
    } else {
      return (
        <View style={[Styles.containerGeneric, {justifyContent:'center'}]}>
          <Text style={Styles.defaultText}>You have no liked recipes</Text>
        </View>
      )
    }
  }

}

module.exports = Liked;