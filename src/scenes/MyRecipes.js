import React, {useEffect, useRef, useState} from 'react';
import { Text, View, FlatList, Image, TouchableOpacity } from 'react-native';
import RBSheet from "react-native-raw-bottom-sheet"; 

import Styles from '../common/Styles';
import Colors from '../common/Colors';
import UserProxy from '../data/UserProxy';
import DataConstants from '../data/DataConstants';

const MyRecipes = ({route, navigation}) => {
 
  let folders = UserProxy.getFolders();
  let [isSubscriber, setIsSubscriber] = useState(UserProxy.getIsSubscribed());
  const recipeTotal = UserProxy.getAllFoldersTotal();
  const {refreshMe} = route.params ? route.params.refreshMe : false;
  const refRBSheet = useRef();

  navigation.setOptions({
    headerRight: () => (
      <TouchableOpacity
        style={{marginRight:20}}
        onPress={() => isSubscriber || recipeTotal < 20 ? refRBSheet.current.open()
          : navigation.navigate('Subscribe', {view:'MyRecipes', viewParams:{param: null}})}
        hitSlop={{top:10, right: 10, bottom: 10, left: 10}}
      >
        <Image source={require('../../img/icon-add.png')} />
      </TouchableOpacity>
    ),
  });  

  // useEffect(() => {
  //   const ref = firestore().collection('users').doc(UserProxy.getUID()).collection('myRecipes').where('folder', '==', 0);
  //   ref.onSnapshot(this.onCollectionUpdate);
  // },[]);

  useEffect(() => {
    const unsubscribe = navigation.addListener('focus', () => {
      //console.log('focus')
      setIsSubscriber(UserProxy.getIsSubscribed());
      if (refreshMe) {
        folders = UserProxy.getFolders();
      }
    });
    return () => unsubscribe();
  }, [navigation, route, folders]);   

  return (
    <View style={{backgroundColor: Colors.slate, flex:1}}>
      {folders && recipeTotal > 0 ?
        <FlatList
          data={folders}
          contentContainerStyle = {Styles.recipeList}
          renderItem = {({item, index}) => {
            //{console.log(item, index)}
            return (
              <TouchableOpacity 
                onPress={() => { 
                  refRBSheet.current.close();
                  isSubscriber || index === 0 ? navigation.navigate('MyRecipesFolder', {folderIndex:index})
                    : navigation.navigate('Subscribe', {view:'MyRecipes', viewParams:{param: null}}) 
                }}>
                <View style={[Styles.folder, {backgroundColor:DataConstants.getFolderColors()[index].color, opacity: isSubscriber || index === 0 ? 1 : 0.5}]}>
                  <Text style={[Styles.defaultTitle, {color: '#FFF'}]}>{item.title.toUpperCase()}</Text>
                  <Text style={[Styles.defaultText, {color: '#FFF', marginTop: 5}]}>{item.total} {item.total === 1 ? 'recipe' : 'recipes'}</Text>
                </View>
              </TouchableOpacity>
            )
          }}
          keyExtractor={(item, index) => index.toString()}
        />  
        :
        <View style={Styles.noRecipeMessage}>
          <Image source={require('../../img/icon-drawer-recipes.png')} style={{marginRight: 20}} />
          <Text style={[Styles.defaultText, {flex: 1}]}>You don’t have any recipes yet! Add one here</Text>      
          <Image source={require('../../img/icon-arrow.png')} />
        </View>
      }

      <RBSheet
        ref={refRBSheet}
        closeOnDragDown={true}
        closeOnPressMask={true}
        height={180}
        customStyles={{
          wrapper: {
            backgroundColor: "rgba(0,0,0,0.2)"
          },
          draggableIcon: {
            backgroundColor: Colors.darkBlue
          }
        }}
      >
        <TouchableOpacity
          onPress={() => {
            refRBSheet.current.close();
            UserProxy.getAllFoldersTotal() <= 10 || subscriber ? navigation.navigate('AddRecipe', {edit:false}) 
              : navigation.navigate('Subscribe', {view:'AddRecipe', viewParams:{edit:false}});
          }}
        >
          <View style={Styles.overlayMenuItem}>
            <Image source={require('../../img/icon-add.png')} style={Styles.recipeAddIcon} />
            <Text style={Styles.defaultTitle}>New Recipe</Text>
          </View>
        </TouchableOpacity>
        <View style={Styles.overlayMenuSeperator} />
        <TouchableOpacity
          onPress={() => {
            refRBSheet.current.close();
            isSubscriber ? navigation.navigate('AddFolder', {title: 'Add Folder'}) 
              : navigation.navigate('Subscribe', {view:'MyRecipes', viewParams:{param: null}})
          }}
        >
          <View style={Styles.overlayMenuItem}>
            <Image source={require('../../img/icon-add.png')} style={Styles.recipeAddIcon} />
            <Text style={Styles.defaultTitle}>New Folder</Text>
          </View>
        </TouchableOpacity>
      </RBSheet>       
    </View>
  )
} 

export default MyRecipes;