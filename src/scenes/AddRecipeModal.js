import React from 'react';
import { Text, View, PixelRatio, TouchableOpacity, Image, ImageBackground, ScrollView, TextInput, Alert, Platform } from 'react-native';
import { KeyboardAwareScrollView } from 'react-native-keyboard-aware-scroll-view'
import { AutoGrowingTextInput } from 'react-native-autogrow-textinput';
import RNTesseractOcr from 'react-native-tesseract-ocr';
import ImagePicker from 'react-native-image-picker';
import Styles from '../common/Styles';
import Colors from '../common/Colors';
import RecipeStorage from '../storage/RecipeStorage';

const {width, height} = require('Dimensions').get('window');

export default class AddRecipeModal extends React.Component {

  constructor(props, context) {
    super(props, context);
    this.state = {
      imgSource: null,
      name: '',
      ingredients: [{quantity:'', ingredient:''}],
      method: '',
      tags: [
        {tag:'Vegan', displayText:'vegan', selected: false, pressed: false},
        {tag:'Vegetarian', displayText:'vegetarian', selected: false, pressed: false},
        {tag:'Low Histermine', displayText:'low histermine', selected: false, pressed: false},
        {tag:'Dairy Free', displayText:'dairy free', selected: false, pressed: false},
        {tag:'Elimination diet', displayText:'elimination', selected: false, pressed: false}
      ],
      tagLabelX:0,
      tagLabelY:0
    };
  }

  render() {
    return (
      <KeyboardAwareScrollView style={{flex:1, backgroundColor:'#fff', borderRadius: 4}}>
        <View style={{flexDirection: 'row'}}>
          <TouchableOpacity
            style={[Styles.button, {marginRight:10}]}
            onPress={this.handleScanPress}
            >
            <Image source={require('../../img/icon-camera.png')} style={{marginRight:12}} />
            <Text style={Styles.buttonText}>Scan</Text>
          </TouchableOpacity>
          <TouchableOpacity
            style={Styles.button}
            onPress={this.handleScanPress}
            >
            <Image source={require('../../img/icon-link.png')} style={{marginRight:12}} />
            <Text style={Styles.buttonText}>URL</Text>
          </TouchableOpacity>
        </View>

        {this.renderImage()}

        <View style={{height: 40}}>
          <Text style={[Styles.defaultTitle, {color: this.state.nameError ? '#ff0000' : '#000'}]}>Name</Text>
        </View>
        <TextInput
          style={[Styles.textInput,
            { marginBottom: this.state.nameError ? 4 : 20,
              paddingBottom:10,
              borderBottomColor: this.state.nameError ? '#ff0000' : '#000'}]}
          value={this.state.name}
          onChangeText={(text) => {
            this.setState({name: text, nameError: false})
          }}
          returnKeyType={'next'}
          underlineColorAndroid='transparent'
          placeholder={'Enter name'} />
        {this.getErrorContainer(this.state.nameError)}

        <View style={{flexDirection: 'row', height: 40, alignItems: 'center'}}>
          <Text style={[Styles.defaultTitle, {color: this.state.nameError ? '#ff0000' : '#000'}]}>Ingredients</Text>
        </View>

        {this.renderIngredients()}
        {this.getErrorContainer(this.state.ingredientsError)}

        <TouchableOpacity
          style={[Styles.buttonRound, {marginTop: 10, marginBottom:20, alignSelf: 'flex-end'}]}
          onPress={() => this.handleNewIngredientPress()}>
          <Image source={require('../../img/icon-add-green.png')} />
        </TouchableOpacity>

        <View style={{height: 40}}>
          <Text style={[Styles.defaultTitle, {color: this.state.methodError ? '#ff0000' : '#000'}]}>Method</Text>
        </View>
        <AutoGrowingTextInput
          style={[Styles.textInput,
            { marginBottom:this.state.methodError ? 4 : 20,
              paddingBottom:10,
              borderBottomColor: this.state.methodError ? '#ff0000' : '#000'}]}
          value={this.state.method}
          onChangeText={(text) => {
            this.setState({method: text, methodError: false})
          }}
          underlineColorAndroid='transparent'
          placeholder={'Enter method'} />
        {this.getErrorContainer(this.state.methodError)}

        <View style={{height: 40}}>
          <Text style={Styles.defaultTitle}>Tags</Text>
        </View>
        <View style={{flexDirection:'row', marginBottom: 20}}>
          {this.renderTags()}
        </View>

        <TouchableOpacity
          style={[Styles.button, {alignSelf:'stretch', justifyContent: 'center', opacity: this.state.loading ? 0.3 : 1}]}
          disabled={this.state.loading ? true : false}
          onPress={() => this.handleSavePress()}>
          <Text style={Styles.buttonText}>Save</Text>
        </TouchableOpacity>

      </KeyboardAwareScrollView>
    )
  }

  getErrorContainer(errorMsg) {
    if (errorMsg) {
      return (
        <View style={{flexDirection: 'row', alignItems: 'center', justifyContent: 'flex-end'}}>
          <Text style={[Styles.defaultText, {color: '#ff0000'}]}>{errorMsg}</Text>
          <View style={Styles.errorIcon}>
            <Text style={[Styles.defaultText], {color: '#fff'}}>!</Text>
          </View>
        </View>
      )
    }
  }

  renderImage() {
    if (this.state.imageData) {
      return <ImageBackground source={this.state.imageData} style={Styles.mainImageModal} />
    } else {
      return (
        <TouchableOpacity style={Styles.mainImageModal} onPress={() => this.selectPhotoTapped()}>
          <Image source={require('../../img/icon-img.png')} />
        </TouchableOpacity>
      )
    }
  }

  renderIngredients() {
    if (this.state.ingredients.length > 0) {
      var that = this;
      return this.state.ingredients.map(function(ingredient, i) {
        return (
          <View style={{flexDirection: 'row'}} key={i}>
            <TextInput
              style={[Styles.textInput, Styles.textInputQuantity, {borderBottomColor: that.state.ingredientsError ? '#ff0000' : '#000'}]}
              // keyboardType={'numeric'}
              value={ingredient.quantity}
              onChangeText={(text) => {
                var tmpData = that.state.ingredients;
                tmpData[i].quantity = text;
                that.setState({ingredients:tmpData})
              }}
              returnKeyType={'next'}
              underlineColorAndroid='transparent'
             />
           <TextInput
             style={[Styles.textInput, Styles.textInputIngredient, {borderBottomColor: that.state.ingredientsError ? '#ff0000' : '#000'}]}
             value={ingredient.ingredient}
             onChangeText={(text) => {
               var tmpData = that.state.ingredients;
               tmpData[i].ingredient = text;
               that.setState({ingredients:tmpData})
             }}
             returnKeyType={'next'}
             underlineColorAndroid='transparent'
            />
          </View>
        )
      });
    }
  }

  renderTags() {
    if (this.state.tags.length > 0) {
      var that = this;
      return this.state.tags.map(function(tag, i){
        return (
          <View key={i} style={{overflow:'visible'}}>
            <TouchableOpacity style={[Styles.tag, {backgroundColor: tag.selected ? Colors.lightGrey : Colors.grey}]}
              onPressIn={(evt) => that.handleTagPress(true, tag.tag, evt, i)}
              onPressOut={(evt) => that.handleTagPress(false, tag.tag, evt, i)}>
              <Text style={[Styles.defaultText, {fontSize:12, color: tag.selected ? '#fff' : '#000'}]}>{tag.displayText}</Text>
            </TouchableOpacity>
          </View>
        )
      })
    }
  }

  handleTagPress(pressed, text, evt, i) {
    //console.log('handleTagPress', pressed, evt.nativeEvent.target)
    // pageX, pageY change based on scrollview position
    var tmpData = this.state.tags;
    if (pressed) {
      tmpData[i].selected = !tmpData[i].selected;
    }
    this.setState({
      tags: tmpData,
      tagLabelText: text,
      tagPressed: pressed,
      tagLabelLeft: Math.floor(evt.nativeEvent.pageX),
      tagLabelTop: Math.floor(evt.nativeEvent.pageY - 90)
    });
  }

  selectPhotoTapped() {
    const options = {
      title: 'Photo Picker',
      takePhotoButtonTitle: 'Take Photo...',
      chooseFromLibraryButtonTitle: 'Choose from Library...',
      quality: 0.5,
      maxWidth: 300,
      maxHeight: 300,
      allowsEditing: false,
      storageOptions: {
        skipBackup: true
      }
    };

    ImagePicker.showImagePicker(options, (response) => {

      if (response.didCancel) {
        console.log('User cancelled photo picker');
      }
      else if (response.error) {
        console.log('ImagePicker Error: ', response.error);
      }
      else if (response.customButton) {
        console.log('User tapped custom button: ', response.customButton);
      }
      else {
        var source;

        // You can display the image using either:
        source = {uri: 'data:image/jpeg;base64,' + response.data, isStatic: true};

        // Or:
        // if (Platform.OS === 'android') {
        //   source = {uri: response.uri, isStatic: true};
        // } else {
        //   source = {uri: response.uri.replace('file://', ''), isStatic: true};
        // }

        this.setState({
          imageData: source
        });
      }
    });
  }

  handleScanPress() {
    console.log('scan pressed')
    const options = {
      title: 'Photo Picker',
      takePhotoButtonTitle: 'Take Photo...',
      chooseFromLibraryButtonTitle: 'Choose from Library...',
      quality: 0.5,
      maxWidth: 300,
      maxHeight: 300,
      allowsEditing: false,
      storageOptions: {
        skipBackup: true
      }
    };

    ImagePicker.showImagePicker(options, (response) => {

      // console.log('Response = ', response);

      if (response.didCancel) {
        console.log('User cancelled photo picker');
      }
      else if (response.error) {
        console.log('ImagePicker Error: ', response.error);
      }
      else if (response.customButton) {
        console.log('User tapped custom button: ', response.customButton);
      }
      else {
        var source;

        // You can display the image using either:
        //source = {uri: 'data:image/jpeg;base64,' + response.data, isStatic: true};

        // Or:
        if (Platform.OS === 'android') {
           source = {uri: response.uri, isStatic: true};
        } else {
           source = {uri: response.uri.replace('file://', ''), isStatic: true};
         }

        /*this.setState({
          scanImageData: source
        });*/

        const tessOptions = {
          whitelist: null,
          blacklist: '1234567890\'!"#$%&/()={}[]+*-_:;<>'
        };

        const isIOS = Platform.OS === 'ios';
        const langName = 'eng';
        const lang = isIOS ? RNTesseractOcr[langName] : langName;

        var imgPath = source.uri;

        // console.log(imgPath)

        /**
         * @param {string} imgPath - The path of the image.
         * @param {string} lang - The language you want to process.
         * @param {object} tessOptions - Tesseract options.
         */
        RNTesseractOcr.recognize(imgPath, langName, tessOptions)
          .then((result) => {
            //this.setState({ ocrResult: result });
            console.log("OCR Result: ", result);
          })
          .catch((err) => {
            console.log("OCR Error: ", err);
          })
          .done();
      }
    });
  }

  handleNewIngredientPress() {
    // console.log('add pressed')
    var tmpData = this.state.ingredients;
    tmpData.push({quantity:'', ingredient:''});
    this.setState({ingredients:tmpData});
  }

  handleSavePress() {
    var nameValid = this.state.name.length >= 1 ? true : false;
    var ingredientsValid = this.state.ingredients.length >= 1 ? true : false;
    var methodValid = this.state.method.length ? true : false;
    this.setState({
      nameError: nameValid ? null : 'Please enter a name',
      ingredientsError: ingredientsValid ? null : 'ingredients',
      methodError: methodValid ? null : 'Please enter a method'
    });
    if (nameValid && ingredientsValid && methodValid) {
      this.setState({loading: true});
      this.saveData();
    }
  }

  async saveData() {
    var recipe = {};
    recipe.name = this.state.name;
    recipe.ingredients = this.state.ingredients;
    recipe.method = this.state.method;
    recipe.tags = this.state.tags;
    recipe.image = this.state.imageData;

    // console.log(recipe)

    RecipeStorage.storeRecipe(recipe)
      .then( (isStored) => {
        this.setState({loading: false});
        this.props.onRequestClose();
      })
      .catch((error) => {
        this.setState({loading: false});
        console.log("SOMETHING WENT WRONG")
        Alert.alert('Error', 'There was an error saving your recipe.')
      });
  }
}

module.exports = AddRecipeModal;
