import React, {useState, useEffect, useContext, useRef} from 'react';
import { Text, View, ImageBackground, TouchableOpacity, Image, Animated } from 'react-native';
import storage from '@react-native-firebase/storage';
import auth from '@react-native-firebase/auth';
import Firebase from '../communication/Firebase';
import ImagePicker from 'react-native-image-picker';
import Swiper from 'react-native-swiper';
import {AuthContext} from '../../App';
import RBSheet from "react-native-raw-bottom-sheet"; 

import Styles from '../common/Styles';
import Colors from '../common/Colors';
import UserProxy from '../data/UserProxy';

const Profile = ({navigation, route}) => {

  const {refreshMe} = route.params ? route.params.refreshMe : false;
  const [imageData, setImageData] = useState(null);
  const isAnonymous = UserProxy.getIsAnonymous();
  const hasUserImage = UserProxy.getUserData().photoURL;
  console.log(UserProxy.getUserData())
  const userID = UserProxy.getUID();
  let screenName = UserProxy.getScreenName();
  const { signOut } = useContext(AuthContext);
  let [isSubscriber, setIsSubscriber] = useState(UserProxy.getIsSubscribed());
  const refRBSheet = useRef();

  navigation.setOptions({
    headerRight: () => (
      !isAnonymous &&
      <TouchableOpacity
        style={{marginRight:20}}
        hitSlop={{top:10, right:10, bottom:10, left:10}}
        onPress={() => refRBSheet.current.open()}
      >
        <Image source={require('../../img/icon-edit-dots.png')} />
      </TouchableOpacity>
    ),  
  });  

  useEffect(() => {
    let isSubscribed = true;
    if (hasUserImage && isSubscribed) {
      const ref = storage().ref(`user-images/${UserProxy.getUID()}.jpg`);
      ref.getDownloadURL().then(data => {
        setImageData({url: data});
      }).catch(error => {
        console.log(error);
      })      
    }
  }),[];

  useEffect(() => {
    const unsubscribe = navigation.addListener('focus', () => {
      //console.log('focus')
      setIsSubscriber(UserProxy.getIsSubscribed());
      if (refreshMe) {
        screenName = UserProxy.getScreenName();
        isAnonymous = UserProxy.getUserData().isAnonymous;
      }
    });
    return () => unsubscribe();
  }, [navigation, route]);     

  const selectPhotoTapped = () => {
    const options = {
      title: 'Photo Picker',
      takePhotoButtonTitle: 'Take Photo...',
      chooseFromLibraryButtonTitle: 'Choose from Library...',
      quality: 0.8,
      maxWidth: 300,
      maxHeight: 300,
      allowsEditing: false,
      storageOptions: {
        skipBackup: true
      }
    };

    ImagePicker.showImagePicker(options, (response) => {
      if (response.didCancel) {
        console.log('User cancelled photo picker');
      }
      else if (response.error) {
        console.log('ImagePicker Error: ', response.error);
      }
      else if (response.customButton) {
        console.log('User tapped custom button: ', response.customButton);
      }
      else {
        var source = {uri: response.uri};
        setImageData(source);
        uploadImageData(source);
      }
    });
  }  

  const uploadImageData = async (imageSource) => {
    Firebase.uploadUserImage(userID, imageSource)
    .then((response) => {
      if (response) {
        //update user data
        Firebase.addProfileImageStatus()
        .then((response) => {
          if (response) {
            console.log(response)
          }
        })
        .catch(e => console.log(e));
      }
    })
    .catch(e => console.log(e));
  }

  const handleLogout = async () => {
    try {
      await auth().signOut()
      signOut();
    } catch (e) {
      console.log(e);
    }
  };  

  const subscriberContent = () => {
    if (isSubscriber) {
      return (
        <View style={{flex:1, alignContent: 'center', justifyContent: 'flex-end'}}>
          <Image source={require('../../img/splash-footer-premium.png')} style={[Styles.profileFooterImage]} />
        </View>
      )
    } else {
      return (
        <View style={{ flex: 1, justifyContent: 'flex-end' }}>
          <Image source={require('../../img/splash-footer-premium.png')} style={Styles.profileFooterImage} />
          <View style={Styles.profileSubscribeContainer}>
            <View style={Styles.profileSubscribeInnerContainer}>
              <Swiper 
                activeDotColor={Colors.darkBlue}
                dotColor={'rgba(83,94,98,0.3)'}
                >
                <View style={Styles.swipeCard}>
                  <Text style={[Styles.defaultTitle, {marginBottom: 5}]}>Collections</Text>
                  <Text style={Styles.defaultText}>Premium curated recipes regularly updated</Text>
                </View>
                <View style={Styles.swipeCard}>
                  <Text style={[Styles.defaultTitle, {marginBottom: 5}]}>Storage</Text>
                  <Text style={Styles.defaultText}>Store unlimited recipes</Text>
                </View>        
                <View style={Styles.swipeCard}>
                  <Text style={[Styles.defaultTitle, {marginBottom: 5}]}>Tools</Text>
                  <Text style={Styles.defaultText}>Upgraded recipe tools</Text>
                </View>                          
              </Swiper>
              <TouchableOpacity
                style={[Styles.button, {alignSelf:'stretch', justifyContent: 'center', height: 50}]}
                onPress={() => navigation.navigate('Subscribe', {view:'Profile', viewParams:{refreshView: true}})}
              >
                <Text style={Styles.buttonText}>Subscribe now</Text>
              </TouchableOpacity>                 
            </View>
          </View>
        </View>
      )
    }
  };

  if (isAnonymous) {
    return (
      <View style={[Styles.authContainer, {justifyContent: 'center'}]}>
        <View style={{flex:1, alignContent: 'center', justifyContent: 'center'}}>
          <Image source={require('../../img/icon-drawer-recipes.png')} style={{tintColor: '#fff', alignSelf: 'center', marginBottom: 15}} />
          <Text style={[Styles.buttonText, {color:'#fff', textAlign: 'center', marginBottom: 20, fontSize: 15}]}>Keep your recipes safe!</Text>
          <TouchableOpacity
            style={[Styles.button, {alignSelf:'stretch', justifyContent: 'center', height: 50, marginHorizontal: 20}]}
            onPress={() => navigation.navigate('ProfileCreateAccount')}>
            <Text style={Styles.buttonText}>Create an account</Text>
          </TouchableOpacity>   
        </View>    
        
        {subscriberContent()}
        
      </View>
    )
  } else {
    return (
      <View style={[Styles.authContainer]}>
        <View style={{padding:20, flex:1}}>
          <TouchableOpacity onPress={() => selectPhotoTapped()}>
            <ImageBackground source={imageData} style={Styles.profileImage} imageStyle={{ borderRadius: 37}}>
              {imageData == null && <Image source={require('../../img/icon-img.png')} />}
            </ImageBackground>
          </TouchableOpacity>          
          <View style={{flexDirection:'row', paddingVertical: 10, marginVertical: 10}}>
            <Text style={[Styles.buttonText, {color:'#fff', fontSize: 15}]}>{UserProxy.getUserData().email}</Text>
          </View>
          <View style={Styles.profileSeparator}></View>
          <TouchableOpacity style={{flexDirection:'row', paddingVertical: 10, marginVertical: 10, alignItems: 'center'}}
            onPress={() => navigation.navigate('ProfileScreenName')}
          >
            <Text style={[Styles.buttonText, {color:'#fff', fontSize: 15, flex: 1}]}>{
              screenName == null ? 'Choose a screen name' : screenName
            }</Text>
            <Image source={require('../../img/icon-forward-small.png')} style={{tintColor: '#fff'}} />
          </TouchableOpacity> 
          <View style={Styles.profileSeparator}></View>
        </View>  

        {subscriberContent()}      

        <RBSheet
          ref={refRBSheet}
          closeOnDragDown={true}
          closeOnPressMask={true}
          height={120}
          customStyles={{
            wrapper: {
              backgroundColor: "rgba(0,0,0,0.2)"
            },
            draggableIcon: {
              backgroundColor: Colors.darkBlue
            }
          }}
        >
          <TouchableOpacity style={Styles.overlayMenuItem}
            onPress={() => handleLogout()}
          >
            <Text style={Styles.defaultTitle}>Logout</Text>
          </TouchableOpacity>       
        </RBSheet>                  
      </View>
    )
  }

}

module.exports = Profile;