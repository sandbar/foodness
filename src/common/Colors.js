'use strict';

let Colors = {
  brightGreen: '#b9f27c', //salmon
  darkBlue: '#535e62', //yellow
  slate: '#D0DBE0',
  darkSlate: '#ADBEC5',
  lightSlate: '#E7EDEF',
  grey: '#eeeeee',
  lightestGrey: '#f0f6f6',
  altLightestGrey: '#e0ecec',
  lightGrey: '#e7edef',
  midGrey: '#adbec5'
};

export default Colors;
