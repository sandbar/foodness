import { StyleSheet, Platform, Dimensions } from 'react-native';
import Colors from './Colors';

const {width, height} = Dimensions.get('window');
const iPhoneXMargin = (Platform.OS === 'ios') && (height === 812 || height === 896) ? 30 : 0;
const isPad = height/width < 1.5 ? true : false;

//const FONT = 'HiraKakuStdN-W8';
//const FONT = 'Hiragino Kaku Gothic StdN W8';
const MEDIUM = '600';
const REGULAR = '400';
const LIGHT = '300';

module.exports = StyleSheet.create({

  containerGeneric: {
    padding: 10,
    flex: 1,
    backgroundColor: Colors.slate,
    alignItems: 'center'
  },
  loadingContainer: {
    padding: 10,
    flex: 1,
    backgroundColor: Colors.brightGreen,
    alignItems: 'center',
    justifyContent: 'center'
  },
  paddedContainer: {
    padding: 20
  },
  button: {
    flexDirection: 'row',
    backgroundColor: Colors.darkBlue,
    borderRadius: 4,
    height: 50,
    alignItems: 'center',
    paddingLeft: 20,
    paddingRight: 20,
    //flex:1
  },
  buttonText: {
    color: '#fff',
    fontSize: 18,
    fontFamily: 'HelveticaNeue-Bold'
    //fontFamily: FONT
  },
  buttonRound: {
    width: 40,
    height: 40,
    borderRadius: 20,
    //backgroundColor: Colors.darkBlue,
    alignItems: 'center',
    justifyContent: 'center'
  },
  defaultText: {
    color: Colors.darkBlue,
    fontSize: 15,
    fontFamily: 'Helvetica Neue'
  },
  smallText: {
    color: Colors.darkBlue,
    fontSize: 12,
    fontFamily: 'Helvetica Neue'
  },  
  defaultTitle: {
    color: Colors.darkBlue,
    fontSize: 18,
    fontFamily: 'HelveticaNeue-Medium'
  },
  defaultMainTitle: {
    fontSize: 22,
    fontFamily: 'HelveticaNeue-Bold'
  },
  textInput: {
    color: Colors.darkBlue,
    borderBottomColor: Colors.darkBlue,
    borderBottomWidth: 2,
    height: 40,
    fontFamily: 'HelveticaNeue-Bold',
    fontSize: 15
  },
  listSeparator: {
    height: 1,
    backgroundColor: Colors.slate
  },

  /** TABBAR **/
  tabBar: {
    backgroundColor: Colors.darkBlue
  },

  /** NAVBAR **/
  navBar: {
    backgroundColor: Colors.darkBlue
  },
  navBarLight: {
    elevation: 0,
    height: 50,
    paddingTop: 10,
    backgroundColor: '#fff',
    borderBottomWidth: 1,
    borderBottomColor: Colors.darkSlate
  },
  headerTitleStyle: {
    color: '#FFF',
    fontSize: 17,
    fontFamily: 'HelveticaNeue-Bold',
    //marginTop: 10
  },  
  rightButtonIconStyle: {
    top: -15 + (iPhoneXMargin),
    right: 5
  },
  leftButtonIconStyle: {
    top: -14 + (iPhoneXMargin),
    left: 10
  },
  addRecipeButton: {
    marginRight: 20
  },

  /** OVERLAY MENU **/
  overlayMenuContainer: {
    position: 'absolute',
    width: width,
    height: height,
    backgroundColor: 'rgba(83,94,98,0.3)'
  },
  overlayMenu: {
    position: 'absolute',
    bottom: 84,
    width: width,
    backgroundColor: '#ffffff',
  },
  overlayMenuItem: {
    width: width,
    height: 70,
    justifyContent: 'center',
    alignItems: 'center'  
  },
  overlayMenuSeperator: {
    width: width,
    height: 2,
    backgroundColor: Colors.slate,
    opacity: 0.4
  },
  recipeAddIcon: {
    width:16, 
    height:16,
    tintColor: Colors.darkSlate,
    position: 'absolute',
    left: 20
  },

  /** OVERLAY NOTIFICATION **/
  overlayNotification: {
    width: width,
    height: 140,
    backgroundColor: Colors.brightGreen,
    alignItems: 'center',
    justifyContent: 'center',
    position: 'absolute',
    top:0
  },

  /** AUTH **/
  authContainer: {
    flex: 1,
    backgroundColor: Colors.darkSlate    
  },
  splashFooter: {
    position: 'absolute',
    bottom: 0,
    width: width,
    height: width * 0.5534
  },

  /** HOME **/
  menuContainer: {
    flex: 1,
    backgroundColor: Colors.slate
  },
  recipeList: {
    backgroundColor: Colors.slate
  },
  recipeListColumn: {
    justifyContent: 'space-between',
    padding: 10
  },
  recipeCard: {
    width: isPad ? (width / 2 ) - 15 : width, 
    marginBottom: 30,
  },
  recipeCardTitle: {
    flex: 0,
    alignSelf: 'flex-start',
    backgroundColor: '#FFFFFF',
    marginRight: 20,
    padding: 10,
    marginTop: 15
  },
  recipeCardTitleText: {
    fontFamily: 'HelveticaNeue-Bold',
    fontSize: 16,
    color: Colors.darkBlue,
  },
  recipeCardImage: {
    width: isPad ? (width / 2) - 15 : width,
    height: isPad ? (width / 2) * 0.6 : width * 0.6, 
    flex: 0
  },
  recipeImageSplash: {
    width: width,
    height: width * 0.6,
  },
  recipeEdit: {
    width: 23,
    height: 23,
    marginRight: 10
  },
  recipeCardLiked: {
    width: 20,
    height: 18,
    marginLeft: 10
  },
  folder: {
    width: width,
    height: 100,
    alignItems: 'center',
    justifyContent: 'center',
    backgroundColor: 'red'
  },
  sharedUserPhoto: {
    width: 24,
    height: 24,
    borderRadius: 12,
    backgroundColor: Colors.brightGreen,
    alignItems: 'center',
    justifyContent: 'center'    
  },
  defaultSharedUserPhoto: {
    width: 17,
    height: 16,
    tintColor: Colors.darkBlue,
  },
  noRecipeMessage: {
    width: width,
    height: 86,
    backgroundColor: Colors.brightGreen,
    flexDirection: 'row',
    padding: 20,
    alignItems: 'center',
    elevation: 1,
    shadowColor: "#000000",
    shadowOpacity: 0.15,
    shadowRadius: 6,
    shadowOffset: {
      height: 3,
      width: 0
    }    
  },

  /** VIEW RECIPE **/
  viewRecipeHeader: {
    position: 'absolute',
    top: 20 + iPhoneXMargin,
    left: 20,
    width: width - 40,
    flexDirection: 'row'
  },

  /** MODAL **/
  modal: {
    flex:1,
    margin: 10,
    padding: 10,
    backgroundColor: '#fff',
    borderRadius: 4
  },
  modalHeader: {
    marginTop: 10,
    marginBottom: 20,
    flexDirection: 'row',
    justifyContent: 'center'
  },
  modalClose: {
    position: 'absolute',
    right: 0
  },
  modalTitle: {
    fontSize: 22
  },

  /** ADD RECIPE **/

  mainImageRecipe: {
    width: width,
    height: width * 0.6,
    justifyContent: 'center',
    alignItems: 'center',
    backgroundColor: Colors.brightGreen
  },
  mainImageModal: {
    width: width - 40,
    height: width * 0.5,
    //backgroundColor: Colors.grey,
    borderRadius: 4,
    justifyContent: 'center',
    alignItems: 'center',
    marginTop: 10,
    marginBottom: 10,
  },
  addRecipeMainButtons: {
    flexDirection: 'row',
    position: 'absolute',
    bottom: 0,
    left: 0,
    padding: 12
  },
  hLine: {
    width: width-40,
    height: 1,
    backgroundColor: Colors.slate,
    marginTop: 10,
    marginBottom: 10
  },
  textInputQuantity: {
    width: (width-40) * 0.2,
    marginRight: 10
  },
  textInputIngredient: {
    width: (width-50) * 0.8
  },
  tag: {
    height: 25,
    borderRadius: 12,
    backgroundColor: Colors.slate,
    alignItems: 'center',
    justifyContent: 'center',
    marginRight: 10,
    paddingHorizontal: 10
  },
  errorIcon: {
    width: 20,
    height: 20,
    backgroundColor: '#ff0000',
    borderRadius: 10,
    justifyContent: 'center',
    alignItems: 'center',
    marginLeft: 6
  },
  backButtonAddRecipe: {
    position: 'absolute',
    top: 22 + (iPhoneXMargin),
    left: 22
  },
  folderIcon: {
    width: 16,
    height: 16,
    marginRight: 10
  },

  /** RECIPE **/

  hLineRecipe: {
    width: width-20,
    height: 1,
    backgroundColor: '#000',
    marginTop: 10,
    marginBottom: 10
  },
  backButton: {
    position: 'absolute',
    top: 22 + (iPhoneXMargin),
    left: 22
  },
  ingredientRow: {
    flexDirection: 'row',
    paddingVertical: 15
  },

  /** COLLECTIONS **/

  collectionsList: {
    paddingVertical: 10,
    backgroundColor: Colors.slate,
    paddingHorizontal: 10
  },
  collectionCard: {
  },
  collectionCardTitle: {
    flex: 0,
    alignSelf: 'flex-start',
    backgroundColor: '#FFFFFF',
    marginRight: 20,
    padding: 10,
    marginTop: 15
  },
  collectionCardTitleText: {
    fontFamily: 'HelveticaNeue-Bold',
    fontSize: 16,
    color: Colors.darkBlue,
  },
  collectionCardImage: {
    backgroundColor: Colors.slate, 
    width: width - 20,
    height: width * 0.4,
    elevation: 1,
    shadowColor: "#000000",
    shadowOpacity: 0.15,
    shadowRadius: 6,
    shadowOffset: {
      height: 3,
      width: 0
    }     
  },
  collectionEdit: {
    width: 23,
    height: 23,
    marginRight: 10
  },  

  /** SUBSCRIBE **/

  subscribeImage: {
    position: 'absolute',
    top: 0,
    left: 0,
    width: width,
    height: width * (282/414)
  },
  subscribeScrollView: {
    flex:1, 
    padding:0, 
    paddingTop: 110, 
    marginTop: width * (282/414) - 110
  },
  subscribeCard: {
    position: 'absolute',
    width: width - 20,
    top: width * (282/414),
    left: 10,
    bottom: 0,
    backgroundColor: Colors.brightGreen,
    paddingHorizontal: isPad ? 200 : 30,
    paddingBottom: isPad ? 100 : 30,
    paddingTop: isPad ? 50 : 30,
    borderTopLeftRadius: 20,
    borderTopRightRadius: 20,
    borderBottomLeftRadius: 0,
    borderBottomRightRadius: 0
  },
  subscribeTick: {
    width: 34,
    height: 34,
    borderRadius: 17,
    alignItems: 'center',
    justifyContent: 'center',
    borderWidth: 1,
    borderColor: Colors.darkBlue,
    marginRight: 15
  },
  subscribeRow: {
    flexDirection:'row', 
    flex: 1,
    alignItems: 'center',
    marginBottom: 20
  },

  /** PROFILE **/

  profileContainer: {
    paddingHorizontal: 20,
    paddingVertical: 30,
    backgroundColor: '#FFFFFF',
    alignItems: 'center',
  }, 
  logoutButton: {
    position: 'absolute', 
    bottom: 20,
    left: 20,
    width: width - 40,
    justifyContent: 'center', 
    height: 50,
    opacity: 0.5
  },
  profileFooterImage: {
    width: width,
    height: width * 0.3973
  },
  profileSubscribeContainer: {
    backgroundColor: Colors.darkBlue,
    paddingHorizontal: 10
  },
  profileSubscribeInnerContainer: {
    backgroundColor: Colors.brightGreen,
    borderTopLeftRadius: 20,
    borderTopRightRadius: 20,
    padding: 10,
    paddingTop: 15,
    alignItems: 'center',
    height: 175
  },
  swipeCard: {
    flex: 1,
    alignItems: 'center'
  },
  profileImage: {
    width: 74,
    height: 74,
    borderRadius: 37,
    backgroundColor: Colors.brightGreen,
    alignItems: 'center',
    justifyContent: 'center'
  },
  profileSeparator: {
    height: 1,
    width: width - 40,
    backgroundColor: '#fff'
  },

  /* SHOPPING LIST */

  hiddenRow: {
    flex: 1,
    flexDirection: 'row',
    justifyContent: 'flex-end',
    alignItems: 'center',
    backgroundColor: 'red',
    paddingHorizontal: 10
  }

});
