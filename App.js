import * as React from 'react';
import {
  ActivityIndicator,
  TouchableOpacity,
  Image,
  StatusBar,
  View,
  Platform
} from 'react-native';

import { NavigationContainer } from '@react-navigation/native';
import { createStackNavigator, HeaderTitle } from '@react-navigation/stack';
import { createBottomTabNavigator } from '@react-navigation/bottom-tabs';
import auth from '@react-native-firebase/auth';
import firestore from '@react-native-firebase/firestore';
import PushNotificationIOS from "@react-native-community/push-notification-ios";

import Styles from './src/common/Styles';
import UserProxy from './src/data/UserProxy';
import Purchase from './src/communication/Purchase';

import Auth from './src/scenes/auth/Auth';
import AnonAuthContinue from './src/scenes/auth/AnonAuthContinue';
import Home from './src/scenes/Home';
import AddRecipe from './src/scenes/AddRecipe';
import ChooseTags from './src/scenes/ChooseTags';
import ChooseFolder from './src/scenes/ChooseFolder';
import AddFolder from './src/scenes/AddFolder';
import MyRecipes from './src/scenes/MyRecipes';
import MyRecipesFolder from './src/scenes/MyRecipesFolder';
import ViewRecipe from './src/scenes/ViewRecipe';
import Collections from './src/scenes/Collections';
import ShoppingList from './src/scenes/ShoppingList';
import CollectionRecipes from './src/scenes/CollectionRecipes';
import Subscribe from './src/scenes/Subscribe';
import Cooking from './src/scenes/Cooking';
import Profile from './src/scenes/Profile';
import ProfileScreenName from './src/scenes/ProfileScreenName';
import ProfileAuth from './src/scenes/ProfileAuth';

export const AuthContext = React.createContext();

export default function App() {

  PushNotificationIOS.requestPermissions();

  const [state, dispatch] = React.useReducer(
    (prevState, action) => {
      switch (action.type) {
        case 'IS_AUTHENTICATED':
          return {
            ...prevState,
            isLoading: false,
            isAuthenticated: true
          };  
        case 'NOT_AUTHENTICATED':
          return {
            ...prevState,
            isLoading: false,
            isAuthenticated: false
          };
      }
    },
    {
      isLoading: true,
      isAuthenticated: false
    }
  );

  const authContext = React.useMemo(
    () => ({
      signIn: () => dispatch({type: 'IS_AUTHENTICATED'}),
      signOut: () => dispatch({type: 'NOT_AUTHENTICATED'}),
      signUp: async data => {
        dispatch({type: 'IS_AUTHENTICATED'})
      }
    }),[]
  );

  React.useEffect(() => {

    //console.log('checking auth state')
    let isSubscribed = true;

    auth().onAuthStateChanged((user) => {
      if (isSubscribed) {
        isSubscribed = false; // we need this line to avoid the useEffect running twice & causing the yellow box error for setting state on an unmounted component
        if (user) {
          const userJson = user.toJSON();

          // get the user data
          const userRef = firestore().collection('users').doc(userJson.uid);

          userRef.get()
            .then((docSnapshot) => {
              if (docSnapshot.exists) {
                userRef.get().then((doc) => {
                  const user = doc.data();
                  // console.log(user)
                  UserProxy.storeUserData(user.userJson);
                  // store liked
                  if (user.myLikedRecipes) {
                    UserProxy.storeLiked(user.myLikedRecipes);
                  }
                  if (user.myFolders) {
                    UserProxy.storeFolders(user.myFolders);
                  }
                  if (user.subscriptionReceipt) {
                    const receiptBody = {
                      'receipt-data': user.subscriptionReceipt.transactionReceipt,
                      'password': '616eeac553ce4fc3a437ae0ed96b68a1'
                    };    
                    Purchase.validateReceipt(receiptBody)
                    .then((response) => {
                      //console.log('validateReceipt', response);
                      const lastTransaction = response.latest_receipt_info.length - 1;
                      const receiptDate = parseInt(response.latest_receipt_info[lastTransaction].expires_date_ms);
                      if (receiptDate > Date.now()) {
                        console.log('subscriber!')
                        UserProxy.setIsSubscribed(true);
                      } else {
                        console.log('expired subscriber')
                        UserProxy.setIsSubscribed(false);
                      }
                    })
                    .catch((e) => {
                      console.log('e');
                    })
                  } else {
                    UserProxy.setIsSubscribed(false);
                  }
                });
                dispatch({ type: 'IS_AUTHENTICATED' });
              } else {
                console.log('error getting user data')
                dispatch({ type: 'NOT_AUTHENTICATED' });
              }
          });
        } else {
          // get the user to sign up or in
          dispatch({ type: 'NOT_AUTHENTICATED' });
        }
      }
    })

    return () => isSubscribed = false;  

  }, []);    


  /*
  React.useEffect(() => {
    
    const signInAnonymously = async () => {

      let credential;

      try {
        credential = await auth().signInAnonymously();
      } catch (e) {
        // problem
      }

      const userJson = credential.user.toJSON();
      UserProxy.storeUserData(userJson);

      // add user to cloud firestore

      const userRef = firestore().collection('users').doc(userJson.uid);

      userRef.get()
        .then((docSnapshot) => {
          if (docSnapshot.exists) {
            console.log('user already exists');
            userRef.get().then((doc) => {
              const user = doc.data();
              // store liked
              if (user.myLikedRecipes) {
                UserProxy.storeLiked(user.myLikedRecipes);
              }
              if (user.myFolders) {
                UserProxy.storeFolders(user.myFolders);
              }
              if (user.subscriptionReceipt) {
                const receiptBody = {
                  'receipt-data': user.subscriptionReceipt.transactionReceipt,
                  'password': '616eeac553ce4fc3a437ae0ed96b68a1'
                };    
                Purchase.validateReceipt(receiptBody)
                .then((response) => {
                  //console.log('validateReceipt', response);
                  const lastTransaction = response.latest_receipt_info.length - 1;
                  const receiptDate = parseInt(response.latest_receipt_info[lastTransaction].expires_date_ms);
                  if (receiptDate > Date.now()) {
                    console.log('subscriber!')
                    UserProxy.setIsSubscribed(true);
                  } else {
                    console.log('expired subscriber')
                    UserProxy.setIsSubscribed(false);
                  }
                })
                .catch((e) => {
                  console.log('e');
                })
              } else {
                UserProxy.setIsSubscribed(false);
              }
              dispatch({ type: 'IS_AUTHENTICATED' });
            });
          } else {
            // add the user to FS
            userRef.set({userJson})
            .then (() => {
              console.log('user added successfully');
              userRef.update({
                myFolders: firestore.FieldValue.arrayUnion({title: 'default', total: 0})
              })
              .then (() => {
                console.log('default folder added successfully')
                UserProxy.storeFolders({title: 'default', total: 0});
                UserProxy.setIsSubscribed(false);
                dispatch({ type: 'IS_AUTHENTICATED' });
              })
              .catch((error) => {
                console.log('error adding folder', error)
              })

            })
            .catch((error) => {
              console.log('error adding user', error)
            })
          }
        });   
    }

    signInAnonymously();
    
  }, []); 

*/

  const RootStack = createStackNavigator();
  const Stack = createStackNavigator();
  const Tab = createBottomTabNavigator();

  function SplashScreen() {
    return (
      <View style={Styles.loadingContainer}>
        <Image source={require('./img/leaves.png')} />
      </View>      
    )
  }

  function LogoTitle() {
    return (
      <Image
        style={{ width: 93, height: 36 }}
        source={require('./img/foodness-logo.png')}
      />
    );
  }

  function AuthStack() {
    return (
      <Stack.Navigator>
        <Stack.Screen name="Auth" component={Auth} options={{headerShown:false}} />
        <Stack.Screen name="AnonAuthContinue" component={AnonAuthContinue} options={{headerShown:false}} />
      </Stack.Navigator>
    )
  }

  function HomeStack() {
    return (
      <Stack.Navigator
        screenOptions={{
          headerStyle:Styles.navBar,
          headerTitleStyle: Styles.headerTitleStyle
        }}
      >
        <Stack.Screen name="Home" component={Home} options={{headerTitle: props => <LogoTitle {...props} />}} />
        <Stack.Screen name="ViewRecipe" component={ViewRecipe} options={{headerShown:false}} />
        <Stack.Screen name="Cooking" component={Cooking} options={{headerShown:false}} />

      </Stack.Navigator>
    )
  }

  function MyRecipesStack() {
    return (
      <Stack.Navigator
        screenOptions={{
          headerStyle:Styles.navBar,
          headerTitleStyle: Styles.headerTitleStyle
        }}
      >
        <Stack.Screen name="MyRecipes" component={MyRecipes} options={{title: 'My Recipes'}} />
        <Stack.Screen name="MyRecipesFolder" component={MyRecipesFolder} />
        <Stack.Screen name="ViewRecipe" component={ViewRecipe} options={{headerShown:false}} />
        <Stack.Screen name="Cooking" component={Cooking} options={{headerShown:false}} />
        <Stack.Screen name="AddRecipe" component={AddRecipe} options={{headerShown:false}} /> 
        <Stack.Screen name="ChooseTags" component={ChooseTags} options={{title: 'Choose Tags'}} />        
        <Stack.Screen name="ChooseFolder" component={ChooseFolder} options={{title: 'Choose Folder'}} />        
        <Stack.Screen name="AddFolder" component={AddFolder} options={{title: 'Add Folder'}} />        
      </Stack.Navigator>
    )
  }  

  function CollectionsStack() {
    return(
      <Stack.Navigator
        screenOptions={{
          headerStyle:Styles.navBar,
          headerTitleStyle: Styles.headerTitleStyle
        }}
      >   
        <Stack.Screen name="Collections" component={Collections} options={{title: 'Collections'}} />
        <Stack.Screen name="CollectionRecipes" component={CollectionRecipes} />
        <Stack.Screen name="ViewRecipe" component={ViewRecipe} options={{headerShown:false}} />
        <Stack.Screen name="Cooking" component={Cooking} options={{headerShown:false}} />
        {/* <Stack.Screen name="Subscribe" component={Subscribe} options={{headerShown:false}} /> */}
      </Stack.Navigator>     
    )
  }

  function ShoppingListStack() {
    return(
      <Stack.Navigator
        screenOptions={{
          headerStyle:Styles.navBar,
          headerTitleStyle: Styles.headerTitleStyle
        }}
      >   
        <Stack.Screen name="ShoppingList" component={ShoppingList} options={{title: 'Shopping List'}} />
      </Stack.Navigator> 
    )
  }

  function ProfileStack() {
    return (
      <Stack.Navigator
        screenOptions={{
          headerStyle:Styles.navBar,
          headerTitleStyle: Styles.headerTitleStyle
        }}
      >   
        <Stack.Screen name="Profile" component={Profile} options={{title: 'Profile'}} />
        {/* <Stack.Screen name="ProfileAnonymous" component={ProfileAnonymous} options={{title: 'Sign up'}} />
        <Stack.Screen name="ProfileSignUpPassword" component={ProfileSignUpPassword} options={{title: 'Choose password'}} />*/}
        <Stack.Screen name="ProfileScreenName" component={ProfileScreenName} options={{title: 'Choose screen name'}} /> 
        <Stack.Screen name="ProfileCreateAccount" component={ProfileAuth} options={{title: 'Create Account'}} /> 

      </Stack.Navigator>       
    )
  }

  function TabNavigator() {
    return (
      <Tab.Navigator
        tabBarOptions = {{
          showLabel: false, 
          tabStyle: Styles.tabBar,
          style: Styles.tabBar,
          safeAreaInset: {bottom: 'never', top: 'never'}
        }}
        screenOptions={({route}) => ({
          tabBarIcon: ({focused}) => {
            if (route.name === 'Home') {
              if (focused) {
                return <Image source={require('./img/icon-tab-home-selected.png')} />
              }
              return <Image source={require('./img/icon-tab-home.png')} />
            } else if (route.name === 'MyRecipes') {
              if (focused) {
                return <Image source={require('./img/icon-tab-myrecipes-selected.png')} />
              }              
              return <Image source={require('./img/icon-tab-myrecipes.png')} />
            } else if (route.name === 'Collections') {
              if (focused) {
                return <Image source={require('./img/icon-tab-collections-selected.png')} />
              }              
              return <Image source={require('./img/icon-tab-collections.png')} />
            } else if (route.name === 'ShoppingList') {
              if (focused) {
                return <Image source={require('./img/icon-tab-shoppinglist-selected.png')} />
              }              
              return <Image source={require('./img/icon-tab-shoppinglist.png')} />
            } else if (route.name === 'Profile') {
              if (focused) {
                return <Image source={require('./img/icon-tab-profile-selected.png')} />
              }              
              return <Image source={require('./img/icon-tab-profile.png')} />
            }  
          } 
        })}
      >
        <Tab.Screen name='Home' component={HomeStack} />
        <Tab.Screen name='MyRecipes' component={MyRecipesStack} />
        <Tab.Screen name='Collections' component={CollectionsStack} />
        <Tab.Screen name='ShoppingList' component={ShoppingListStack} />
        <Tab.Screen name='Profile' component={ProfileStack} />
      </Tab.Navigator>
    )
  }

  function MainStackScreen() {

    if (state.isLoading) {
      return <SplashScreen />
    }

    return (
      <AuthContext.Provider value={authContext}>
        <Stack.Navigator>
        {state.isAuthenticated ?
          ( <Stack.Screen name='TabNavigator' component={TabNavigator} options={{headerShown:false}} /> )
          :
          ( <Stack.Screen name='Auth' component={AuthStack} options={{headerShown:false}} /> )
        }
        </Stack.Navigator>   
      </AuthContext.Provider> 
    )
  }

  return (
    <NavigationContainer>
      <RootStack.Navigator mode="modal">
        <RootStack.Screen name='Main' component={MainStackScreen} options={{ headerShown: false}} />
        <RootStack.Screen name='Subscribe' component={Subscribe} options={{ headerShown: false}} />
      </RootStack.Navigator>
    </NavigationContainer>
  )
};