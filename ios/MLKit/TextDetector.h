//
//  TextDetector.h
//  foodness
//
//  Created by Seamus on 28/05/2018.
//  Copyright © 2018 Facebook. All rights reserved.
//

#import <Foundation/Foundation.h>

@class FIRVisionDocumentTextRecognizer;

@interface TextDetector : NSObject <RCTBridgeModule>

@property (nonatomic, strong) FIRVisionDocumentTextRecognizer *textDetector;

@property (nonatomic, strong) RCTResponseSenderBlock callback;

- (void)detect:(NSString *)imageName withCallback:(RCTResponseSenderBlock)callback;

@end
