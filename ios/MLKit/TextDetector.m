//
//  TextDetector.m
//  foodness
//
//  Created by Seamus on 28/05/2018.
//  Copyright © 2018 Facebook. All rights reserved.
//
@import Firebase;
#import <React/RCTBridgeModule.h>
#import "TextDetector.h"

@implementation TextDetector

+ (BOOL)requiresMainQueueSetup
{
  return YES;
}

RCT_EXPORT_MODULE();

- (instancetype)init
{
  self = [super init];
  if (self) {
    self.textDetector = [[FIRVision vision] cloudDocumentTextRecognizer];
  }
  return self;
}

RCT_EXPORT_METHOD(detect:(NSString *)imageName withCallback:(RCTResponseSenderBlock)callback)
{
  dispatch_async(dispatch_get_global_queue( DISPATCH_QUEUE_PRIORITY_HIGH, 0), ^{
    self.callback = callback;
    __weak typeof(self) weakSelf = self;
    UIImage* image = [[UIImage alloc] initWithContentsOfFile:imageName];

    [_textDetector processImage:[[FIRVisionImage alloc] initWithImage:image]
                      completion:^(FIRVisionDocumentText *cloudText, NSError *error) {
                        if (error != nil) {
                          NSLog(@"Error: %@", error.localizedDescription);
                          weakSelf.callback(@[error, @"no text"]);

                        } else if (cloudText != nil) {
                          NSLog(@"Result: %@", cloudText.text);
                          weakSelf.callback(@[[NSNull null], cloudText.text]);
                        } else {
                          weakSelf.callback(@[[NSNull null], @"no text"]);
                        }
                      }];
  });
}

@end
